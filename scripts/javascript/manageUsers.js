/**
 * Created by Dero on 4/13/2016.
 */

$(document).ready(function(){

    $('#submit').on('click', function(e) {
        e.preventDefault();

        pullUsers();
    });

    $('#search').on('click', function(e) {
        e.preventDefault();

        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType:'json',
            data: {
                controller:'Users',
                action:'pullUsers',
                userEmail: $('#username_search').val()
            },
            success: function (data) {
                if(data.success)
                {
                    $('#alterForm').html('');
                    $('#alterForm').html(data.data.form);
                    // console.log(data);
                    $("#delbutton").on('click', function(e) {
                        e.preventDefault();
                        deleteUser();
                    });
                }
                else
                {
                    alert(data.errormsg);
                }
            }
        });

        pullUsers();
    });

    $('#submitNewUser').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType:'json',
            data: {
                controller:'Users',
                action:'newUser',
                data:{
                    organization : $("#SelectOrganization").find(":selected").val(),
                    role : $("#role").find(":selected").val(),
                    first_name : $("#first_name").val(),
                    middle_name : $("#middle_name").val(),
                    last_name : $("#last_name").val(),
                    email : $("#email").val()
                }
            },
            success: function (data) {
                if(data.success)
                {
                  $('#userModal').modal('hide');
                }
                else
                {
                    alert(data.errormsg);
                }
            }
        });
    })
})

function deleteUser(){
    // e.preventDefault();
    var user_id = $('#user_id').val();
    if(confirm('Do you want to remove this user?'))
    {
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType:'json',
            data: {
                controller:'Users',
                action:'delete',
                data:{
                    user_id : user_id
                }
            },
            success: function (data) {
                if(data.success)
                {
                    $('#user_id').val(0);
                    $('#SelectUser option[value="' + $('#username').val() + '"]').remove();
                    $('#username').val('deleted');
                }
                else
                {
                    alert(data.errormsg);
                }
            }
        });
    }
}
function findUsersforOrg() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Users',
            action:'pullUsersForOrg',
            organization: $('#SelectOrganization').find(":selected").val()
        },
        success: function (data) {
            if (data.success) {
                var parent = $("#SelectUser");
                parent.html('');

                var optG = document.createElement("optgroup");
                optG.label = $('#SelectOrganization').find(":selected").val();
                optG.id = "assignedUsers";
                var opt;
                var results = data.data.users;
                for (row in results) {
                    opt = document.createElement("option");
                    opt.value = results[row].username;
                    opt.textContent = results[row].username;
                    optG.appendChild(opt);
                }
                parent.append(optG);


                optG = document.createElement("optgroup");
                optG.label = "Unassigned Users";
                results = data.data.unassignedUsers;
                for (row in results) {
                    opt = document.createElement("option");
                    opt.value = results[row].username;
                    opt.textContent = results[row].username;
                    optG.appendChild(opt);
                }
                parent.append(optG);

            } else {
                alert(data.errormsg);
            }
        }
    })
}

function pullUsers() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Users',
            action:'pullUsers',
            userEmail: $('#SelectUser').find(":selected").val()
        },
        success: function (data) {
            if(data.success)
            {
                $('#alterForm').html('');
                $('#alterForm').html(data.data.form);
                // console.log(data);
                $("#delbutton").on('click', function(e) {
                    e.preventDefault();
                    deleteUser();
                });
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
};
