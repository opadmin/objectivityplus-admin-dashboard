/**
 * Created by Dero on 4/13/2016.
 */
$(document).ready(function(){
    
    $('#submit').on('click', function(e) {
        e.preventDefault();
        if ($("#SelectQuestion").find(":selected").val() == "undefined") {
            Selector();
        } else {
            switch ($("#SelectPage").find(":selected").val()) {
                case "FAQ":
                    $.ajax({
                        method: "POST",
                        url: '/dashboard/OPAPI/index.php',
                        dataType:'json',
                        data: {
                            controller:'Manager',
                            action:'generateFAQForm',
                            data:{
                                selection : $("#SelectQuestion").find(":selected").val()
                            }
                        },
                        success: function (data) {
                            if(data.success)
                            {
                                $("#insertForm").html('');
                                $("#insertForm").html(data.data.form);
                            }
                            else
                            {
                                alert(data.errormsg);
                            }
                        }
                    });
                    break;
                case "Team":
                    $.ajax({
                        method: "POST",
                        url: '/dashboard/OPAPI/index.php',
                        dataType:'json',
                        data: {
                            controller:'Manager',
                            action:'generateBioForm',
                            data:{
                                selection : $("#SelectQuestion").find(":selected").val()
                            }
                        },
                        success: function (data) {
                            if(data.success)
                            {
                                $("#insertForm").html('');
                                $("#insertForm").html(data.data.form);
                            }
                            else
                            {
                                alert(data.errormsg);
                            }
                        }
                    });
                    break;
            }
        }


    });
})

function Selector() {
    switch ($('#SelectPage').find(":selected").val()) {
        case "FAQ":
            pullQuestions();
            break;
        case "Team":
            pullTeam();
            break;
    }
}

function pullQuestions() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Manager',
            action:'pullQuestions',
            data:{
                selection : $('#SelectPage').find(":selected").val()
            }
        },
        success: function (data) {
            if(data.success)
            {
                $("#SecondLabel").text("Select Question");
                $("#SelectQuestion").html('');
                var results = data.data;
                var opt;
                for (row in results) {
                    opt = document.createElement('option');
                    opt.textContent = results[row].Question;
                    opt.id = results[row].Question;
                    $("#SelectQuestion").append(opt);
                }
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });

}

function pullTeam() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Manager',
            action:'pullTeam',
            data:{
                selection : $('#SelectPage').find(":selected").val()
            }
        },
        success: function (data) {
            if(data.success)
            {
                $("#SecondLabel").text("Select Team Member");
                $("#SelectQuestion").html('');
                var results = data.data;
                var opt;
                for (row in results) {
                    opt = document.createElement('option');
                    opt.textContent = results[row].Question;
                    opt.id = results[row].Question;
                    $("#SelectQuestion").append(opt);
                }
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
}