/**
 * Created by Dero on 4/13/2016.
 */
$(document).ready(function(){

    $('#submit').on('click', function(e) {
        e.preventDefault();
        Selector();
    });

    bindSubmit2();
    bindTicketSelector();
    bindEditForm();
    bindComments();

    $('#comments').html('');

})


function bindSubmit2() {
    $('#submit2').on('click', function(e) {
        e.preventDefault();
        NewTicket();
    });
}

function bindEditForm() {
    $('#submitEdit').on("click", function(e) {
        e.preventDefault();
        editTicket();
    })

    $('#delete').on("click", function(e) {
        e.preventDefault();
        deleteTicket();
    })
}

function bindTicketSelector() {
    $('.ticket').on("click", function(e) {
        e.preventDefault();
        editTicketForm(this.id);
    })
}

function bindComments() {
    $('#newComment').on("click", function(e) {
        e.preventDefault();
        submitComment();
    })
}

function Selector() {
    if ($('#radios-0').is(':checked')) {
        NewTicketForm();
    }
    if ($('#radios-1').is(':checked')) {
        getTickets();
    }
}

function NewTicketForm() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Ticketing',
            action:'newTicketForm'
        },
        success: function (data) {
            if(data.success)
            {
                $("#alterForm").html('');
                $("#form2").html('');
                $("#alterForm").html(data.data);
                bindSubmit2();
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
}

function NewTicket() {

    console.log("DERO");
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Ticketing',
            action:'newTicket',
            data:{
                username : $('#user_name').val(),
                email : $('#email').val(),
                message : $('#message').val()
            }
        },
        success: function (data) {
            if(data.success)
            {
                NewTicketForm();
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
}

function getTickets() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Ticketing',
            action:'getTickets'
        },
        success: function (data) {
            if(data.success)
            {
                $("#alterForm").html('');
                $("#alterForm").html(data.data);
                bindTicketSelector();
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
}

function editTicketForm(targetID) {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Ticketing',
            action:'editTicketForm',
            data: {
              id :targetID
            },
        },
        success: function (data) {
            if(data.success)
            {
                $("#form2").html('');
                $("#form2").html(data.data);
                bindEditForm();
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });

    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Ticketing',
            action:'getComments',
            data: {
                id :targetID
            },
        },
        success: function (data) {
            if(data.success)
            {
                $("#comments").html('');
                $("#comments").html(data.data);
                bindComments();
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
}

function editTicket() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Ticketing',
            action:'editTicket',
            data : {
                id : $('#id').val(),
                username : $('#username').val(),
                email : $('#email').val(),
                open_date : $('#opendate').val(),
                closed_date : $('#close_date').val(),
                status : $('#status').find(":selected").val(),
                cause : $('#cause').find(":selected").val(),
                solved_by : $('#solved_by').val(),
                message : $('#message').val()
            }
        },
        success: function (data) {
            if(data.success)
            {
                $("#form2").html('');
                getTickets();
                alert("Ticket has been altered.");
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
}

function deleteTicket() {
    if (confirm("Are you sure you want to delete this ticket?")) {
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType:'json',
            data: {
                controller:'Ticketing',
                action:'deleteTicket',
                data : {
                    id : $('#id').val()
                }
            },
            success: function (data) {
                if(data.success)
                {
                    $("#form2").html('');
                    getTickets();
                    alert("Ticket has been deleted.");
                }
                else
                {
                    alert(data.errormsg);
                }
            }
        });
    }
}

function submitComment() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Ticketing',
            action:'submitComment',
            data: {
                comment :$('#comment').val(),
                id : $('#id').val()
            },
        },
        success: function (data) {
            if(data.success)
            {
               getTickets();
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
}
