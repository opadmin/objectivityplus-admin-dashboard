/**
 * Created by Dero on 4/14/2016.
 */
$(document).ready(function(e) {
    $("#submit2").on("click", function(e) {
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType:'json',
            data: {
                controller:'Manager',
                action:'pullScenarios',
                data:{
                    organization : $("#SelectOrganization").find(":selected").val(),
                    startDate : $("#start").val(),
                    endDate : $("#end").val()
                }
            },
            success: function (data) {
                if(data.success)
                {
                    $("#insertHere").html('');
                    $("#insertHere").html(data.data);
                    bindModal();
                }
                else
                {
                    alert(data.errormsg);
                }
            }
        });
    });

    $("#submit").on("click", function(e) {
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType:'json',
            data: {
                controller:'Manager',
                action:'pullIncompleteScenarios',
                data : {
                    unknown : $("#FilterResults-0").prop("checked"),
                    paid : $("#FilterResults-1").prop("checked"),
                    needsPayment : $("#FilterResults-2").prop("checked"),
                    cancelled : $("#FilterResults-3").prop("checked")
                }
            },
            success: function (data) {
                if(data.success)
                {
                    $("#insertHere").html('');
                    $("#insertHere").html(data.data);
                }
                else
                {
                    alert(data.errormsg);
                }
            }
        });
    });

    bindModal();
    bindModalSubmit()

})

function bindModal() {
  $(".edit").on("click", function(e) {
      e.preventDefault();
      $.ajax({
          method: "POST",
          url: '/dashboard/OPAPI/index.php',
          dataType:'json',
          data: {
              controller:'Manager',
              action:'generateEditModal',
              data : {
                  id : $(this).parents('.ScenarioBlock').attr('id')
              }
          },
          success: function (data) {
              if(data.success)
              {
                  $("#insertModalForm").html('');
                  $("#insertModalForm").html(data.data);
                  $('#editModal').modal('show');
                  bindModalSubmit();
              }
              else
              {
                  alert(data.errormsg);
              }
          }
      });
  });
}

function bindModalSubmit() {
  $("#editTheSession").on("click", function(e) {
      e.preventDefault();
      $.ajax({
          method: "POST",
          url: '/dashboard/OPAPI/index.php',
          dataType:'json',
          data: {
              controller:'Manager',
              action:'editSession',
              data : {
                  id : $("#id").attr('value'),
                  status : $("#status").attr('value'),
                  start_datetime : $("#start_datetime").attr('value'),
                  end_datetime : $("#end_datetime").attr('value'),
                  location : $("#location").attr('value')
              }
          },
          success: function (data) {
              if(data.success)
              {
                  $('#editModal').modal('hide');
              }
              else
              {
                  alert(data.errormsg);
              }
          }
      });
  });
}
