$(document).ready(function(e) {
  $("#messageToggle").bootstrapSwitch();

  $('#messageToggle').on('switchChange.bootstrapSwitch', function (event, state) {
    console.log("state change");
      getMessageText();
  });

  $('#submitDates2').on('click', function(e) {
      e.preventDefault();
      $('#Sessions').html('');
      getSelected();
  })

  function getSelected() {
      $('#Part3').fadeIn( "slow", function() {}).removeClass("hidden");

      $('#insertSelection option').each(function(index){
          if($(this).is(':selected'))
          {
              // selected.push($('#insertSelection option').eq(index).val());
              (function() {
                $.ajax({
                    method: "POST",
                    url: '/dashboard/OPAPI/index.php',
                    dataType:'json',
                    data: {
                        controller:'Conferencing',
                        action:'getSessions',
                        data:{
                            organization : $("#SelectOrganization").find(":selected").val(),
                            date : ($('#insertSelection option').eq(index).val()),
                            id : ($('#insertSelection option').eq(index).attr("data-plan"))
                        }
                    },
                    success: function (data) {
                        if(data.success)
                        {

                            $('#Sessions').append(data.data);
                        }
                        else
                        {
                            alert(data.errormsg);
                        }
                    }
                });
              })()
          }
      });
  }
})
