/**
 * Created by Dero on 4/15/2016.
 */

$(document).ready(function(e) {

    if ($("#SelectOrganization").length > 0) {
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType:'json',
            data: {
                controller:'Users',
                action:'pullOrganizations'
            },
            success: function (data) {
                if(data.success)
                {

                    $("#SelectOrganization").html('<option>No Selection</option>');

                    for (var i = 0; i < data.data.length; ++i) {
                        var opt = "<option id='" + data.data[i].id + "'>" + data.data[i].name + "</option>";
                        $("#SelectOrganization").append(opt);
                    }
                    $('#orgSpinner').hide();
                }
                else
                {
                    alert(data.errormsg);
                }
            }
        });
    }

})
