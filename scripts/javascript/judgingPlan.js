/**
 * Created by Dero on 5/13/2016.
 */
$(document).ready(function(e) {

    $('#submit').on('click', function(e) {
        e.preventDefault();
        if (!$.trim( $('#planSelector').html() ).length) {
            $.ajax({
                method: "POST",
                url: '/dashboard/OPAPI/index.php',
                dataType:'json',
                data: {
                    controller:'Manager',
                    action:'pullJudgingPlans',
                    data: {
                        create: $("#choices-1").prop("checked"),
                        approve: $("#choices-0").prop("checked"),
                        runningScoring: $("#choices-2").prop("checked"),
                        cancel: $("#choices-3").prop("checked"),
                        complete: $("#choices-4").prop("checked"),
                        organization : $("#SelectOrganization").find(":selected").val()
                    }
                },
                success: function (data) {
                    if(data.success)
                    {
                        $("#planSelector").html('');
                        $("#planSelector").html(data.data);
                    }
                    else
                    {
                        alert(data.errormsg);
                    }
                }
            });
        } else {
            $.ajax({
                method: "POST",
                url: '/dashboard/OPAPI/index.php',
                dataType:'json',
                data: {
                    controller:'Manager',
                    action:'judgingPlanEditForm',
                    data: {
                        id : $("#selection").find(":selected").val()
                    }
                },
                success: function (data) {
                    if(data.success)
                    {
                        $("#alterForm").html('');
                        $("#alterForm").html(data.data);
                        bindChanges()
                    }
                    else
                    {
                        alert(data.errormsg);
                    }
                }
            });
        }        
    })

    bindChanges()
})

function bindChanges() {
    $('#submitChanges').on('click', function(e) {
        e.preventDefault();

        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType:'json',
            data: {
                controller:'Manager',
                action:'editJudgingPlan',
                data: {
                    id : $("#id").val(),
                    courseTitle : $('#courseTitle').val(),
                    courseNumber : $('#courseNumber').val(),
                    directorID : $('#directorID').val(),
                    status : $("#status").find(":selected").val()
                }
            },
            success: function (data) {
                if(data.success)
                {
                    alert("Plan updated");
                }
                else
                {
                    alert(data.errormsg);
                }
            }
        });
    })
}

function reloadPlans() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Manager',
            action:'pullJudgingPlans',
            data: {
                create: $("#choices-1").prop("checked"),
                approve: $("#choices-0").prop("checked"),
                runningScoring: $("#choices-2").prop("checked"),
                cancel: $("#choices-3").prop("checked"),
                complete: $("#choices-4").prop("checked"),
                organization : $("#SelectOrganization").find(":selected").val()
            }
        },
        success: function (data) {
            if(data.success)
            {
                $("#planSelector").html('');
                $("#planSelector").html(data.data);
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
}