/**
 * Created by Dero on 4/19/2016.
 */
var days = [];
var IDs = [];
// var daysBackups = [];
var username = '';
var conn;

$(document).ready(function(e) {

    //initially hide the toggel for SMS view
    $('#toggle').hide();

    //initialize datepicker
    $('#start, #end').datepicker();

    //set currentDate as the initial enddate of the dateselect range
    $("#end").datepicker('setDate', new Date());

    //show modal for joining a socket group
    $("#socketModal").modal("show");

    //Modal for finding a user already assigned to a scenario
    $('#findUser').on("click", function(e) {
        $('#searchModal').modal("show");
    });

    //search for users already assigned to a scenario
    $('#search').on("click", function(e) {
        e.preventDefault();

        $(this).prop("disabled", true).html("<i class='fa fa-spin fa-spinner'></i>");

        var startDate = $('#start').datepicker({
            dateFormat: 'mm/dd/yyyy'
        }).val();
        var endDate = $('#end').datepicker({
            dateFormat: 'mm/dd/yyyy'
        }).val();

        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType: 'json',
            data: {
                controller: 'Conferencing',
                action: 'findUser',
                data: {
                    organization: $("#SelectOrganization").find(":selected").val(),
                    last_name: $("#last_name1").val(),
                    first_name: $("#first_name1").val(),
                    startDate: startDate,
                    endDate: endDate
                }
            },
            success: function(data) {
                $("#search").prop("disabled", false).html("Search");

                if (data.success) {
                    $('#searchModal').modal('hide');
                    if (data.data !== '') {
                        swal("Located", ($("#first_name1").val() + " is scheduled for: " + data.data));
                    } else {
                        swal({
                            title: "Not Found",
                            text: "Could not locate any scheduled users matching " + $("#first_name1").val() + ($("#last_name1").val() !== "" ? " " + $("#last_name1").val() : "") + ".",
                            type: "error"
                        });
                    }

                } else {
                    swal("Error", data.errormsg, "error");
                }
            }
        });
    });

    //prevent enter key from fucking up forms, specifically the socket modal
    $('form').on('keydown', function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    //establish a new socket conenction and set a username if a user selects to enter a GroupUser
    //TODO: Establish a list of existing group and allow users to enter specific groups
    $('#GroupUser').on("click", function(e) {
        e.preventDefault();
        username = $('#socketUserName').val();

        conn = new WebSocket('ws://objectivityplus.com:8080');
        conn.onopen = function(e) {
            console.log("Connection established!");
        };


        conn.onmessage = function(e) {
            var array = JSON.parse(e.data);
            var msg = array[0]; // days array or #storedUsers html, depending on $.isArray() result
            var f = array[1]; //username

            if ($.isArray(msg)) {
                days = clone(msg);

                var currentDay = $("#Tabs").children(".btnCurrent").index();
                var currentList = getCurrentList(currentDay);

                loadColumns(days[currentDay][1], currentList);
                if (typeof(f) == 'string') {
                    Notify(f, " has modified the schedule");
                }
            } else {
                $('#storedUsers').html(msg);
                if (typeof(f) == 'string') {
                    Notify(f, " has stored a user");
                }
            }
        };
    });

    //submit intial form to locate existing session dates
    $('#submit').on('click', function(e) {

        $(this).prop("disabled", true).html("<i class='fa fa-spin fa-spinner'></i>");

        e.preventDefault();
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType: 'json',
            data: {
                controller: 'Conferencing',
                action: 'pullScenarioDays',
                data: {
                    organization: $("#SelectOrganization").find(":selected").val(),
                    startDate: $('#start').datepicker({
                        dateFormat: 'mm/dd/yyyy'
                    }).val(),
                    endDate: $('#end').datepicker({
                        dateFormat: 'mm/dd/yyyy'
                    }).val()
                }
            },
            success: function(data) {

                $("#submit").prop("disabled", false).html("Submit");

                if (data.success) {
                    $("#insertSelection").html('');
                    $("#insertSelection").html(data.data);

                    $('#Part2, #Part2line').fadeIn("slow", function() {}).removeClass("hidden");
                } else {
                    swal("Error", data.errormsg, "error");
                }
            }
        });
    });

//currently disabled 'submit all' button.
    $('#submit3').on('click', function(e) {
        e.preventDefault();
        saveArray(function() {
            submitArray(true);
        });
    });

    //Add user from new user modal inside tool
    $('#submitNewUser').on('click', function(e) {

        $(this).prop("disabled", true).html("<i class='fa fa-spin fa-spinner'></i>");

        e.preventDefault();
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType: 'json',
            data: {
                controller: 'Users',
                action: 'newUser',
                data: {
                    organization: $("#SelectOrganization").find(":selected").val(),
                    role: $("#role").find(":selected").val(),
                    first_name: $("#first_name").val(),
                    middle_name: $("#middle_name").val(),
                    last_name: $("#last_name").val(),
                    email: $("#email").val(),
                    phone: $("#phone").val()
                }
            },
            success: function(data) {

                $("#submitNewUser").prop("disabled", false).html("Submit");

                if (data.success) {
                    $('#userModal').modal('hide');
                    var usr = $('<div class="col-md-3 smpadding person Draggable neutral" id="' + data.data + '">' + $("#last_name").val() + ', ' + $("#first_name").val() + '</div>');
                    $('#userList').append(usr);

                    $("#insertModalForm").find("input").val("");
                } else {
                    swal("Error", data.errormsg, "error");
                }
            }
        });
    });

    // $('#loadBackup').on('click', function(e) {
    //     $('#backups').html('');
    //
    //     for (var i = 0; i < daysBackups.length; ++i) {
    //         var d = daysBackups[i].datetime;
    //         var datestring = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear() + "  " +
    //             d.getHours() + ":" + d.getMinutes() + "::" + d.getSeconds();
    //
    //         var backup = $("<div id='" + i + "' class='col-md-12 mdPadTop mdPadBot'>" + datestring + "</div>");
    //         $('#backups').append(backup);
    //         $(backup).on("click", function() {
    //             $('#backupModal').modal('hide');
    //         });
    //     }
    //
    //     $('#backupModal').modal('show');
    // });

    // $('#backups').on('click', "> div", function(e) {
    //     var newDay = clone(daysBackups[($(this).attr("id"))].data);
    //     days = newDay;
    //
    //     var currentDay = $("#Tabs").children(".btnCurrent").index();
    //     var currentList = getCurrentList(currentDay);
    //
    //     loadColumns(days[currentDay][1], currentList);
    // });

    $('#submitDates, #reset').on('click', function(e) {
        e.preventDefault();

        $(this).prop("disabled", true).html("<i class='fa fa-spin fa-spinner'></i> Loading conferencing data...");

        $("#limit").slideUp('fast', function() {

            $('#Sessions').html('');

            var selected = getSelected();
            days = [];
            generateTool(selected);
        });


    });

    $('.fa-arrow-circle-right, .fa-arrow-circle-left').on('click', function(e) {
        var that = this;
        saveArray(function() {

            var currentDay = $("#Tabs").children(".btnCurrent").index();
            var currentList = 0;

            for (var i = 0; i < days[currentDay][1].length; ++i) {
                if ($("#Scenario0").children(".memberList").attr("id") == $(days[currentDay][1][i]).attr("id")) {
                    currentList = i;
                    break;
                }
            }

            if ($(that).hasClass('fa-arrow-circle-right')) {
                if (currentList + ($(".memberList").length) >= days[currentDay][1].length) {
                    swal("No later sessions to display");
                } else {
                    loadColumns(days[currentDay][1], currentList + 4);
                }
            } else {
                if (currentList == 0) {
                    swal("No earlier sessions to display");
                } else {
                    loadColumns(days[currentDay][1], currentList - 4);
                }
            }
        });
    });

    $("body").on("DOMNodeInserted", ".studentDrop", function() {

        $(".studentDrop").droppable({
            hoverClass: "studentDropHighlight",
            accept: ".Draggable",
            drop: function(event, ui) {
                if (!$(ui.draggable).hasClass("edited")) {
                    $(ui.draggable).addClass("edited");
                }
                $(ui.draggable).appendTo($(this).siblings(".memberList"));
                $(ui.draggable).removeClass("col-md-4 col-md-3 lgPadLeft neutral instructor smpadding col-md-10 col-md-offset-1 neutral");
                $(ui.draggable).addClass("col-md-11 student");
                if (!($(ui.draggable).children('fa').length > 0)) {
                    $(ui.draggable).append('<i class="fa fa-check-square-o check icon hidden" aria-hidden="true"></i><i class="fa fa-times-circle-o x icon hidden" aria-hidden="true"></i><div class="Attendance"><div class="Present">Present</div><div class="Absent">Absent</div></div>');
                }

                setTimeout(function() {
                    saveArray(function() {

                        submitArray(false);

                        if (typeof(conn) !== "undefined") {
                            conn.send(JSON.stringify([days, username]));
                            conn.send(JSON.stringify([($('#storedUsers').html()), username]));
                        }
                    });
                }, 0);
            }
        });
    });

    $("body").on("DOMNodeInserted", ".instructorDrop", function() {

        $(".instructorDrop").droppable({
            hoverClass: "instructorDropHighlight",
            accept: ".Draggable",
            drop: function(event, ui) {

                var newDiv = $(ui.draggable).clone();
                if (!newDiv.hasClass("edited")) {
                    newDiv.addClass("edited");
                }
                newDiv.removeClass("col-md-4 col-md-3 lgPadLeft neutral student smpadding col-md-10 col-md-offset-1 neutral");
                newDiv.addClass("col-md-11 instructor");
                if (!(newDiv.children('fa').length > 0)) {
                    newDiv.append('<i class="fa fa-check-square-o check icon hidden" aria-hidden="true"></i><i class="fa fa-times-circle-o x icon hidden" aria-hidden="true"></i><div class="Attendance"><div class="Present">Present</div><div class="Absent">Absent</div></div>');
                }
                newDiv.appendTo($(this).siblings(".memberList"));

                setTimeout(function() {
                    saveArray(function() {

                        submitArray(false);

                        if (typeof(conn) !== "undefined") {
                            conn.send(JSON.stringify([days, username]));
                            conn.send(JSON.stringify([($('#storedUsers').html()), username]));
                        }

                    });
                }, 0);

            }
        });
    });

    $("body").on("DOMNodeInserted", "#userList", function() {
        $(".Draggable").draggable({
            helper: 'clone',

            containment: 'body',
            zIndex: 11,
            start: function(event, ui) {

            },
        });
    });


    $("#storedUsers").droppable({
        hoverClass: "storeDropHighlight",
        accept: ".Draggable",
        drop: function(event, ui) {
            $(ui.draggable).addClass("edited col-md-10 col-md-offset-1 neutral");
            $(ui.draggable).removeClass("col-md-3 student instructor smpadding");
            $(ui.draggable).appendTo($('#storedUsers'));

            saveArray(function() {

                submitArray(false);

                if (typeof(conn) !== "undefined") {
                    conn.send(JSON.stringify([($('#storedUsers').html()), username]));
                    conn.send(JSON.stringify([days, username]));
                }

            });
        }
    });

    $("#removeDrop").droppable({
        hoverClass: "removeDropHighlight",
        accept: ".Draggable",
        drop: function(event, ui) {

            // get user name
            var removedUserName = ($(ui.draggable).clone().children().remove(".Attendance").parent().text() || $(ui.draggable).text());
            removedUserName = removedUserName.split(", ")[1] + " " + removedUserName.split(", ")[0];

            swal({
                title: 'Are you sure you want to remove "' + removedUserName + '" from this schedule?',
                text: 'You will not be able to recover "' + removedUserName + '"!',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#f9761d",
                confirmButtonText: "Yes, remove them!",
                closeOnConfirm: false
            },
            function() {
                $(ui.draggable).remove();
                saveArray(function() {

                    submitArray(false);

                    sendToSocket();

                });


                swal("Deleted!", removedUserName + ' has been removed', "success");
            });
        }
    });

    $("body").on("mouseenter", ".Present, .Absent", function() {
        if ($(this).parent().hasClass("student")) {
            $(this).parent().attr("style", "background: rgba(18, 75, 181, .3) !important");
        } else {
            $(this).parent().attr("style", "background: rgba(201, 93, 22, .3) !important");
        }
    });

    $("body").on("mouseleave", ".Present, .Absent", function() {
        $(this).parent().attr("style", "");
    });


    $("body").on("click", ".Present, .Absent", function() {
      //Check if 'Present' was clicked
        if ($(this).hasClass("Present")) {
          //See if 'Absent' was already selected and hide it if it was
            if ((!$(this).parent().siblings(".x").hasClass("hidden"))) {
                $(this).parent().siblings(".x").toggleClass("hidden");
                saveArray(function() {

                    submitArray(false);

                    sendToSocket();
                });
            }
            //Show 'present'
            $(this).parent().siblings(".check").toggleClass("hidden").css("color", "#0ac650");
            //Hide the person after a short delay - Added for ENA 2016
            $(this).parent().parent().delay( 2000 ).slideUp( 300 );
            saveArray(function() {

                submitArray(false);

                sendToSocket();

            });
        } else {
          //See if 'Present' was already selected and hide it if it was
            if ((!$(this).parent().siblings(".check").hasClass("hidden"))) {
                $(this).parent().siblings(".check").toggleClass("hidden")
                saveArray(function() {

                    submitArray(false);

                    sendToSocket();

                });
            }
            //Show 'Absent'
            $(this).parent().siblings(".x").toggleClass("hidden").css("color", "#BC0922");
            saveArray(function() {

                submitArray(false);

                sendToSocket();

            });
        }
    });

    $("body").on("click", ".smsButton", function(e) {
        var success = false;
        $('#Sessions').children().each(function(e) {
            $.ajax({
                method: "POST",
                url: '/dashboard/OPAPI/index.php',
                dataType: 'json',
                data: {
                    controller: 'Conferencing',
                    action: 'sendSMS',
                    data: {
                        session: $(this).attr("data-id"),
                        message: $("#message").val(),
                        time: $(this).parent().parent().justtext()
                    }
                },
                success: function(data) {
                    if (data.success) {
                        success = true;
                    } else {
                        alert(data.errormsg);
                    }
                }
            });
        });

        if (success) {
            swal("SMS messages sent");
        } else {
            swal("SMS messages not sent", error);
        }
    });

    $("body").on("click", ".mailButton", function(e) {
        var success = false;
        var that = $(this);
        $('#Sessions').children().each(function() {
            console.log($(this).parent().parent().justtext());
            (function(current) {
                $.ajax({
                    method: "POST",
                    url: '/dashboard/OPAPI/index.php',
                    dataType: 'json',
                    data: {
                        controller: 'Conferencing',
                        action: 'sendEmail',
                        data: {
                            session: $(current).attr("data-id"),
                            message: $("#message").val(),
                            time: $(that).parent().parent().justtext()
                        }
                    },
                    success: function(data) {
                        if (data.success) {} else {
                            alert(data.errormsg);
                        }
                    }
                });
            })($(this));
        });
        if (success) {
            swal("SMS messages sent");
        } else {
            swal("SMS messages not sent", data.errormsg);
        }
    });

    //open new session modal and insert a new session
    $("body").on("click", ".newSession", function(e) {
      var date = $("#Tabs").children(".btnCurrent").attr('data-id');
      var judgingPlan = $("#Tabs").children(".btnCurrent").attr('data-plan');

      newSessionButton();
    });

    $('#instructionsToggle').on('change',function(){
        if($(this).is(':checked'))
        {
            $('.instruction').fadeIn();
            $('[for="instructionsToggle"]').html('Hide Instructions');
            $('#Part1, #Part2').addClass('row');
        }
        else
        {
            $('.instruction').fadeOut();
            $('[for="instructionsToggle"]').html('Show Instructions');
            $('#Part1, #Part2').removeClass('row');
        }
    });

    $.extend($.expr[":"], {
        "containsAnyCase": function(elem, i, match, array) {
            return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });


    $('#findUserInput').on('input propertychange',function(e){
        var searchTerm = $(this).val();
        $("#userList div").show().not("#userList div:containsAnyCase('" + searchTerm +"')").hide();
    });

    $('#userListShowAllToggle').on('change',function(){
        if($(this).is(':checked'))
        {
            $('[data-jps]').removeClass('hidden');
        }
        else
        {
            $('[data-jps]').addClass('hidden');
        }
    })

    function sendToSocket() {
      //check to see if the logging in user is in a group before sending
        if (typeof(conn) !== "undefined" && username != "") {
            conn.send(JSON.stringify([days, username]));
            conn.send(JSON.stringify([($('#storedUsers').html()), username]));
        }
    }

    function submitArray(showAlert) {

        showAlert = typeof(showAlert) === "undefined" || showAlert;

        var d = new Date();

        for (var i = 0; i < days.length; ++i) {
            (function(i) {
                for (var j = 0; j < days[i][1].length; ++j) {
                    var sessArray = [];
                    sessArray.push("Session:" + $(days[i][1][j]).attr("id"));
                    (function(sessArray) {
                        $(days[i][1][j]).children().each(function() {
                            if ($(this).hasClass('instructor')) {
                                sessArray.push("I:" + $(this).attr("id"));
                            }
                            if ($(this).hasClass('student')) {
                                sessArray.push("S:" + $(this).attr("id"));
                            }
                            if ($(this).hasClass('sessionTitle')) {
                                sessArray.push("DateTime:" + $(this).attr("data-datetime"));
                                sessArray.push("Location:" + $(this).attr("data-location"));
                            }
                        });
                        IDs[i][j] = sessArray;
                    })(sessArray);
                }
            })(i);
        }

        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType: 'json',
            data: {
                controller: 'Conferencing',
                action: 'submitSessions',
                data: {
                    IDs: IDs
                }
            },
            success: function(data) {
                if (data.success) {
                    if (showAlert) {

                        swal("Good news!", "Your sessions are saved!", "success")
                    }
                } else {
                    swal(
                        'Oh dear...Look what happened!',
                        data.errormsg,
                        'error'
                    );
                    // alert(data.errormsg);
                }
            }
        });
    }

    function saveArray(callback) {
        var currentDay = $("#Tabs").children(".btnCurrent").index();
        // var currentPlan = $("#Tabs").children(".btnCurrent").attr("data-plan");
        var currentList = getCurrentList(currentDay);

        (function() {

            if (currentList >= 0) {
                for (var i = currentList; i < (currentList + $("#Scenarios").find(".memberList").length); ++i) {
                    (function(index) {
                        days[currentDay][1][index] = $('#Scenario' + (index % 4)).children(".memberList")[0].outerHTML;
                        if (index == (currentList + ($("#Scenarios").find(".memberList").length - 1))) {
                            //Force end of execution to prevent appending extra sessions   <- Sledgehammer
                            // i = (currentList + $("#Scenarios").find(".memberList").length);
                            i = 100;
                            if (typeof(callback) === "function") {
                                callback();
                                callback = null;
                            }
                        }
                    })(i);
                }
            }
        })();
    }

    //get array of selected dates for modifying
    function getSelected() {
        var selected = [];

        $('#insertSelection option').each(function(index) {
            if ($(this).is(':selected')) {
                selected.push([$('#insertSelection option').eq(index).val(), $('#insertSelection option').eq(index).attr("data-plan"), $('#insertSelection option').eq(index).text()]);
            }
        });

        return selected;
    }

    function generateTool(selected) {
        var selections = selected.length;

        for (var i = 0; i < selections; ++i) {
            days[i] = [];
            IDs[i] = [];
        }

        $('#Tabs').html('');

        for (var i = 0; i < selections; ++i) {

            (function(i) {
                days[i][0] = selected[i][0];
                days[i][2] = selected[i][1];
                var tab = $("<button data-plan='" + selected[i][1] + "' data-id='" + selected[i][0] + "' id='" + selected[i][0].replace(/\//g, '') + "' class='confButton btn btn-opblue'>" + selected[i][2].replace(", ", "<br/>") + "</button>");
                $('#Tabs').append(tab);

                $(tab).on("click", function() {
                    var that = this;
                    saveArray(function() {
                        setCurrent($(that), true);
                    });
                });
            })(i);
        }

        loadArray(days);

    }

    function loadArray(days) {
        for (var i = 0; i < days.length; ++i) {
            (function(i) {

                $.ajax({
                    method: "POST",
                    url: '/dashboard/OPAPI/index.php',
                    dataType: 'json',
                    data: {
                        controller: 'Conferencing',
                        action: 'loadColumns',
                        data: {
                            date: days[i][0],
                            id: days[i][2]
                        }
                    },
                    success: function(data) {
                        if (data.success) {
                            days[i][1] = data.data;

                            $(".Draggable").draggable({
                                helper: 'clone',

                                containment: 'body',
                                zIndex: 11,
                                start: function(event, ui) {


                                }
                            });

                            if (i == days.length - 1) {
                                setCurrent($(".confButton[data-id='" + days[0][0] + "'][data-plan='" + days[0][2] + "']"), true);
                                loadUserList();
                            }

                        } else {
                            alert(data.errormsg);
                        }
                    }
                });
            })(i);
        }
    }

    function loadColumns(target, start) {
        $('.ScenarioCol').empty();

        var count = 0;

        try {

            for (var i = (start || 0); i <= target.length; ++i) {
                var id = '#Scenario' + (count);
                ++count;

                if (!($(id + '> .instructorDrop').length > 0)) {
                    $(id).append('<div class="instructorDrop"></div>');
                }
                if (!($(id + '> .studentDrop').length > 0)) {
                    $(id).append('<div class="studentDrop"></div>');
                }

                $(id).append(target[i]);
            }


            $(".Draggable").draggable({
                helper: 'clone',

                containment: 'body',
                zIndex: 11,
                start: function(event, ui) {


                },
            });

        }

        catch (e) {
            swal({
                title: "Error",
                text: "There was an error loading the scenarios. Please reload the page.",
                confirmButtonText: "Reload",
                type: "error"
            }, function() {
                location.reload();
            });
        }

    }

    function loadUserList() {
        $.ajax({
            method: "POST",
            url: '/dashboard/OPAPI/index.php',
            dataType: 'json',
            data: {
                controller: 'Conferencing',
                action: 'pullUncommittedUsers',
                data: {
                    organization: $("#SelectOrganization").find(":selected").val()
                }
            },
            success: function(data) {
                if (data.success) {
                    $('#userList').html('');
                    $('#userList').html(data.data);

                    $(".Draggable").draggable({
                        helper: 'clone',

                        containment: 'body',
                        zIndex: 11,
                        start: function(event, ui) {

                        },
                    });

                    setTimeout(function() {

                        $('#Part3, #Part3line, #limit, #toggle').removeClass("hidden").slideDown();
                        $('#unwantedUsers').slideDown();

                        $("#submitDatesLoader").addClass("hidden");

                        $("#submitDates").prop("disabled", false).html("Submit");

                    }, 0);

                    newSessionButton();

                } else {
                    swal("Error", data.errormsg, "error");
                }
            }
        });
    }

    function setCurrent(target, flag) {
        $(".confButton").removeClass('btnCurrent');
        $(target).addClass('btnCurrent');
        $(target).blur();

        /* flag == true means it's a fresh load and the tool
        will display the results on the first day in the list */
        if (flag == true) {
            for (var i = 0; i < days.length; ++i) {
                if (days[i][0] == target.attr("data-id") && days[i][2] == target.attr("data-plan")) {
                    loadColumns(days[i][1], 0);
                }
            }
        }
    }

    function getCurrentList(currentDay) {
        var currentList = '';

        for (var i = 0; i < days[currentDay][1].length; ++i) {
            if ($("#Scenario0").children(".memberList").attr("id") == $($(days[currentDay][1][i])).attr("id")) {
                currentList = i;
                break;
            }
        }

        return currentList;

    }

    function clone(obj) {
        if (obj == null || typeof(obj) != 'object')
            return obj;
        var temp = obj.constructor();
        for (var key in obj)
            temp[key] = clone(obj[key]);
        return temp;
    }
+
    function Notify(name, message) {
        $.notify({
            // options
            icon: 'glyphicon glyphicon-warning-sign',
            message: (name + message),
        }, {
            // settings
            type: "info",
            allow_dismiss: true,
            newest_on_top: false,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    }

    jQuery.fn.justtext = function() {

        return $(this).clone()
            .children()
            .remove()
            .end()
            .text();

    };

    function newSessionButton() {
      $('.ScenarioCol:not(:has(>.memberList))').first().html('').append("<i class='newSession' style='color: #00678f; line-height: 597px; font-size: 8em;' class='fa fa-plus-square fa-4x' aria-hidden='true'></i>");
    }

    function convertUTCDateToLocalDate(date) {
        var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();

        newDate.setHours(hours - offset);

        return newDate;
    }

});
