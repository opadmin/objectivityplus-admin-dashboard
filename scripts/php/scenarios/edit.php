<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 3/21/2016
 * Time: 9:28 AM
 */


$id = $_POST["id"];

$_host = "db2.apexinnovations.com";

$connection = new PDO("mysql:host={$_host};dbname=appquantumdb;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');
$connectionQ = new PDO("mysql:host={$_host};dbname=Quantum;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');

/**     Enable exceptions    **/
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$connectionQ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$command = $connectionQ->prepare(' INSERT INTO Quantum..modified_sessions
                                   SELECT "edit", NOW(), sess.id, sess.start_datetime, sess.end_datetime, sess.version, sess.status, sess.location, sess.invoice_id
                                        FROM appquantumdb.judging_plan_session				sess
                                        WHERE sess.id = :id;'
                                );

$command->execute(array(":id"=>$id));

$command = $connection->prepare(' UPDATE appquantumdb.judging_plan_session		sess
                                        SET sess.start_datetime = NULL, sess.end_datetime = NULL, status = "deleted"
                                        WHERE sess.id = :id;'
                                );

$command->execute(array(":id"=>$id));

echo "success";

header("Location:../../admin/scenarios.php");
