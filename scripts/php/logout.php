<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 1/25/2016
 * Time: 12:44 PM
 */

session_unset();

session_destroy();

header("Location:../../index.php");