<?php
/**
 * Created by PhpStorm.
 * User: dero
 * Date: 3/14/2016
 * Time: 8:19 AM
 */


function getNavigation() {

    echo '<nav class="navbar nopadding OPBlue">
                <!-- Collect the nav links, forms, and other content for toggling -->
            <ul class="nav navbar-nav fullWidth">
	    <li style="margin-left: 7% !important;" class="col-md-2 text-center OPBlue "><a class="whiteText" href="/dashboard/home.php"><i class="fa fa-home fa-2x whiteText"></i><br>Home</a></li>
		<li class="col-md-2 dropdown text-center OPBlue nopadding dropdown-toggle">
		    <a class="whiteText " role="button" aria-haspopup="true" aria-expanded="false" id="admin" data-toggle="dropdown">
		        <i class="fa fa-home fa-2x whiteText"></i><br> &nbsp;Admin<span class="caret"></span>
            </a>

			<ul class="dropdown-menu" role="menu">
				<li class="OPBlue" role="presentation" ><a role="menuitem" href="/dashboard/admin/upload.php"><i class="fa fa-cloud-upload fa-lg whiteText"></i> Mass Upload Tool</a></li>
				<li class="OPBlue" role="presentation" ><a role="menuitem" href="/dashboard/admin/downloads.php"><i class="fa fa-download fa-lg whiteText"></i> Downloads</a></li>
                <li class="OPBlue" role="presentation" ><a role="menuitem" href="/dashboard/admin/website.php"><i class="fa fa-sitemap fa-lg whiteText"></i> Site Management</a></li>
                <li class="OPBlue" role="presentation" ><a role="menuitem" href="/dashboard/admin/users.php"><i class="fa fa-users fa-lg whiteText"></i> Manage Users</a></li>
                <li class="OPBlue" role="presentation" ><a role="menuitem" href="/dashboard/admin/scenarios.php"><i class="fa fa-university fa-lg whiteText"></i> Manage Scenarios</a></li>
                <li class="OPBlue" role="presentation" ><a role="menuitem" href="/dashboard/admin/credits.php"><i class="fa fa-envelope fa-lg whiteText"></i> Send Notifications</a></li>
                <li class="OPBlue" role="presentation" ><a role="menuitem" href="/dashboard/admin/judgingPlan.php"><i class="fa fa-star fa-lg whiteText"></i> Judging Plans</a></li>
                <li class="OPBlue" role="presentation" ><a role="menuitem" href="/dashboard/admin/debrief.php"><i class="fa fa-paint-brush fa-lg whiteText"></i>Debrief IT</a></li>
			</ul>
		</li>
		<li class="col-md-2 text-center OPBlue nopadding"><a class="whiteText" href="/dashboard/ticketing.php"><i class="fa fa-ticket fa-2x whiteText"></i><br>Ticketing</a></li>
        <li class="col-md-2 text-center OPBlue nopadding"><a class="whiteText" href="/dashboard/debrief.php"><i class="fa fa-step-backward fa-2x whiteText smPadTop"></i><br>Debrief IT</a></li>
		<li class="col-md-2 text-center OPBlue nopadding"><a class="whiteText" href="/dashboard/conferencing.php"><i class="fa fa-briefcase fa-2x whiteText smPadTop"></i><br>Conference Tool</a></li>
	</ul>
	</nav>';

}

function getLowerNav() {
    echo '<div class="row mdPadBot">
                <div class="mdPadTop">
                    <div class="btn-group btn-group-lg col-lg-offset-4">
                        <submit type="submit" class="btn btn-default"><a id="Logout" href="/dashboard/scripts/php/logout.php">&nbsp;&nbsp;&nbsp;Logout&nbsp;&nbsp;&nbsp;</a></submit>
                        <submit type="submit" class="btn btn-default"><a id="AdminSite" href="https://appquantum.objectivityplus.com/login">Admin Site</a></submit>
                        <submit type="submit" class="btn btn-default"><a id="AdminSite" href="http://laraveldev.objectivityplus.com">Admin Site (Beta)</a></submit>
                    </div>
                </div>
            </div>';
}
