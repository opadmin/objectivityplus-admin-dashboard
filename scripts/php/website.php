<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 3/24/2016
 * Time: 10:31 AM
 */


function baseForm()
{

    if (isset($_POST['SelectPage']) && $_POST['SelectPage'] == "Team") {
        echo '<!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="SelectPage">Select Page</label>
                    <div class="col-md-4">
                    <select id="SelectPage" name="SelectPage" class="form-control" onchange="Selector()">
                            <option selected value="Team">Team</option>
                            <option value="FAQ">FAQ</option>
                        </select>
                    </div>
                </div>';
    } elseif (isset($_POST['SelectPage']) && $_POST['SelectPage'] == "FAQ") {
        echo '<!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="SelectPage">Select Page</label>
                    <div class="col-md-4">
                        <select id="SelectPage" name="SelectPage" class="form-control" onchange="Selector()">
                            <option value="Team">Team</option>
                            <option selected value="FAQ">FAQ</option>
                        </select>
                    </div>
                </div>';
    } else {
        echo '<!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="SelectPage">Select Page</label>
                    <div class="col-md-4">
                        <select id="SelectPage" name="SelectPage" class="form-control" onchange="Selector()">
                            <option selected value="None">No Selection</option>
                            <option value="Team">Team</option>
                            <option value="FAQ">FAQ</option>
                        </select>
                    </div>
                </div>';
    }

}