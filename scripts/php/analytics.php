<?php
/**
 * Created by PhpStorm.
 * User: dero
 * Date: 3/17/2016
 * Time: 11:37 AM
 */


$_host = "db2.apexinnovations.com";

$connection = new PDO("mysql:host={$_host};dbname=appquantumdb;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');

/**     Enable exceptions    **/
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$command = $connection->prepare('SELECT org.name, count(sess.id) as count, SUBSTRING(sess.start_datetime, 6, 2) as months
                                    FROM appquantumdb.judging_plan_session				sess
                                        INNER JOIN appquantumdb.judging_plan				plan
                                            ON	sess.judging_plan_id = plan.id
                                        INNER JOIN appquantumdb.organization				org
                                            ON	plan.organization_id = org.id
                                        INNER JOIN appquantumdb.scenario					scen
                                            ON	plan.scenario_id = scen.id
                                    GROUP BY org.name, months
                                    ORDER BY org.name, months;');

$scenarios = $command->fetchAll(PDO::FETCH_ASSOC);

json_encode($scenarios);
