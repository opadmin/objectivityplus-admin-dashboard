<?php
/**
 * Created by PhpStorm.
 * User: dero
 * Date: 3/16/2016
 * Time: 12:00 PM
 */

$_host = "db2.apexinnovations.com";
$connection = new PDO("mysql:host={$_host};dbname=appquantumdb;", 'opadmin', 'A5rEwombo2i4wWekdfrF');

/**     Enable exceptions    **/
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION) or die ('Can not connect to database');
$id = $_POST["user_id"];

if (isset($_POST["first_name"])) {
    $first_name = $_POST["first_name"];
    updateFirst($first_name, $id, $connection);
}
if (isset($_POST["middle_name"])) {
    $middle_name = $_POST["middle_name"];
    updateMiddle($middle_name, $id, $connection);
}
if (isset($_POST["last_name"])) {
    $last_name = $_POST["last_name"];
    updateLast($last_name, $id, $connection);
}
//if (isset($_POST["address_1"])) {
//    $address1 = $_POST["address_1"];
//    updateAddress1($address1, $id, $connection);
//}
//if (isset($_POST["address_2"])) {
//    $address2 = $_POST["address_2"];
//    updateAddress2($address2, $id, $connection);
//}
//if (isset($_POST["city"])) {
//    $city = $_POST["city"];
//    updateCity($city, $id, $connection);
//}
//if (isset($_POST["State"])) {
//    $state = $_POST["State"];
//    updateState($state, $id, $connection);
//}
//if (isset($_POST["zip"])) {
//    $zip = $_POST["zip"];
//    updateZip($zip, $id, $connection);
//}
if (isset($_POST["phone"])) {
    $phone = $_POST["phone"];
    updatePhone($phone, $id, $connection);
}
header('Location: /dashboard/admin/users.php');


function updateFirst($first_name, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.user_main usr
                                        SET usr.first_name = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$first_name));

}

function updateMiddle($middle_name, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.user_main usr
                                        SET usr.middle_name = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$middle_name));

}

function updateLast($last_name, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.user_main usr
                                        SET last_name = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$last_name));

}

function updateAddress1($address1, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.contact_street_address ctc
                                        INNER JOIN appquantumdb.user_main usr ON usr.contact_info_id = ctc.id
                                        SET address1 = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$address1));

}

function updateAddress2($address2, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.contact_street_address
                                        INNER JOIN appquantumdb.user_main usr ON usr.contact_info_id = ctc.id
                                        SET address1 = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$address2));

}

function updateCity($city, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.contact_street_address
                                        INNER JOIN appquantumdb.user_main usr ON usr.contact_info_id = ctc.id
                                        SET address1 = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$city));

}

function updateState($state, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.contact_street_address
                                        INNER JOIN appquantumdb.user_main usr ON usr.contact_info_id = ctc.id
                                        SET address1 = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$state));

}

function updateZip($zip, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.contact_street_address
                                        INNER JOIN appquantumdb.user_main usr ON usr.contact_info_id = ctc.id
                                        SET address1 = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$zip));

}

function updatePhone($phone, $username, $connection) {
    $command = $connection->prepare('UPDATE appquantumdb.user_main usr
                                        SET phone_number = :param
                                        WHERE usr.id = :username;');

    $command->execute(array("username"=>$username, "param"=>$phone));

}
