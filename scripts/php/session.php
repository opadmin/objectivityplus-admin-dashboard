<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 4/11/2016
 * Time: 10:36 AM
 */

session_start();
if (isset($_SESSION['login_user']) && $_SESSION['login_user'] != 1) {
    header('Location: /dashboard/index.php');
}