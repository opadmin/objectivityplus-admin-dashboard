<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 2/4/2016
 * Time: 8:17 AM
 */

function getScores() {
    try {

        $org = $_POST["SelectOrganization"];
        $start = $_POST["start"];
        $end = $_POST["end"];

        $_host = "db2.apexinnovations.com";

        $connection = new PDO("mysql:host={$_host};dbname=appquantumdb;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');

        /**     Enable exceptions    **/
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $command = $connection->prepare('SELECT sess.id, sess.start_datetime, sess.end_datetime, plan.courseTitle, scen.name
                                            FROM appquantumdb.judging_plan_session				sess
                                                INNER JOIN appquantumdb.judging_plan				plan
                                                    ON	sess.judging_plan_id = plan.id
                                                INNER JOIN appquantumdb.organization				org
                                                    ON	plan.organization_id = org.id
                                                INNER JOIN appquantumdb.scenario					scen
                                                    ON	plan.scenario_id = scen.id
                                            WHERE org.name = :organization
                                            AND start_datetime > :date1
                                            AND	end_datetime < :date2;');

        $command->execute(array(":organization"=>$org, ":date1"=>$start, ":date2"=>$end));

        $scenarios = $command->fetchAll(PDO::FETCH_ASSOC);

        foreach ($scenarios as $row) {
            $id = $row['id'];
            $name = $row['name'];
            $startDate = $row['start_datetime'];
            $endDate = $row['end_datetime'];
            $scenario = $row['name'];
            $judging_plan = $row['courseTitle'];

            echo '<div class="ScenarioBlock col-md-offset-0 col-sm-offset-1 col-xs-offset-1 col-md-3 col-sm-4 col-xs-9 text-left">
                    <h4>' . $name . '</h4>
                    <ul style="list-style-type: none">
                        <li><strong>Start Date</strong>: ' . $startDate . '</li>
                        <li><strong>End Date</strong>: ' . $endDate . '</li>
                        <li><strong>Organization</strong>: ' . $name . '</li>
                        <li><strong>Scenario</strong>: ' . $scenario . '</li>
                        <li><strong>Session ID</strong>: ' . $id . '</li>
                    </ul>
                    <div class="btn-group btn-group-justified mdPadBot">
                        <a type="submit" class="btn btn-default">Score Now</a>
                        <a type="submit" class="btn btn-default">View Scores</a>
                    </div>
              </div>';
        }
    }
    catch (Exception $e){
        $message = 'Database error. Please try again later';
    }

}

function primaryForm() {
    echo '<div class="form-group lgPadTop">
                <label class="control-label col-sm-2" for="org">Select Organization:</label>
                <div class="col-sm-10">
                    <select class="form-control" id="SelectOrganization" name="SelectOrganization">
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6">
                    <label class="control-label col-sm-3" for="start">Start Date:</label>
                    <input type="date" id="start" name="start">
                </div>

                <div class="col-md-6">
                    <label class="control-label col-sm-3" for="end">End Date:&nbsp;</label>
                    <input type="date" id="end" name="end">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button id="submit2" class="btn btn-primay">Submit</button>
                </div>
            </div>';
}

function filterResults() {

    switch($_POST['Plan']) {
        case "Edit/View/Delete Scenarios":
            primaryForm();

            echo '<div class="row">
                        <div class="fullWidth OPOrange text-center whiteText smPadBot smPadTop">
                            <h4>Use the boxes below to edit a scenario</h4>
                        </div>
                    </div>';

            break;
        case "Past Due, Incomplete Scenarios":
            echo '<!-- Multiple Checkboxes -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="FilterResults">Filter Results</label>
                <div class="col-md-4">
                    <div class="checkbox">
                        <label for="FilterResults-0">
                            <input type="checkbox" name="FilterResults" id="FilterResults-0" value="Unknown">
                            Unknown
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="FilterResults-1">
                            <input type="checkbox" name="FilterResults" id="FilterResults-1" value="Paid">
                            Paid
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="FilterResults-2">
                            <input type="checkbox" name="FilterResults" id="FilterResults-2" value="Needs Payment">
                            Needs Payment
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="FilterResults-3">
                            <input type="checkbox" name="FilterResults" id="FilterResults-3" value="Cancelled">
                            Cancelled
                        </label>
                    </div>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <button id="submit" name="singlebutton" class="btn btn-primary">Submit</button>
                </div>
            </div>';

            echo '<div class="row">
                        <div class="fullWidth OPOrange text-center whiteText smPadBot smPadTop">
                            <h4>Use the boxes below to view the filtered scenarios</h4>
                        </div>
                    </div>';
            break;
    }


}

function loadPastDue() {
    $_host = "db2.apexinnovations.com";
    $connection = new PDO("mysql:host={$_host};dbname=appquantumdb;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');

    /**     Enable exceptions    **/
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = 'SELECT org.name, sess.id, sess.end_datetime, sess.status, sess.invoice_id
                FROM 	  appquantumdb.judging_plan_session					sess
                INNER JOIN appquantumdb.judging_plan						jud
                    ON sess.judging_plan_id = jud.id
                INNER JOIN appquantumdb.organization						org
                    ON	jud.organization_id = org.id
                LEFT JOIN appquantumdb.judging_plan_session_result			res
                    ON	  sess.id = res.session_id
                WHERE res.session_id IS NULL';


    if(isset($_POST['FilterResults-0'])) {
        $query = $query . ' AND  sess.status IS NULL ';
    } elseif (isset($_POST['FilterResults-1'])) {
        $query = $query . ' AND  sess.status = \'Paid\' ';
    } elseif (isset($_POST['FilterResults-2'])) {
        $query = $query . ' AND  sess.status = \'Needs Payment\' ';
    } elseif (isset($_POST['FilterResults-3'])) {
        $query = $query . ' AND  sess.status = \'Cancelled\' ';
    }

    $query = $query . ' ORDER BY sess.end_datetime DESC;';

    $command = $connection->prepare($query);
    $command->execute();
    $pastScenarios = $command->fetchAll(PDO::FETCH_ASSOC);

    foreach ($pastScenarios as $row) {
        echo '<div class="ScenarioBlock col-md-offset-0 col-sm-offset-1 col-xs-offset-1 col-md-3 col-sm-4 col-xs-9 text-left" id="' . $row['id'] . '">
                    <h4>' . $row['name'] . '</h4>
                    <ul style="list-style-type: none">
                        <li><strong>End Date</strong>: ' . $row['end_datetime'] . '</li>
                        <li><strong>Organization</strong>: ' . $row['name'] . '</li>
                        <li><strong>Status</strong>: ' . $row['status'] . '</li>
                        <li><strong>Session ID</strong>: ' . $row['id'] . '</li>
                        <li><strong>Invoice ID</strong>: ' . $row['invoice_id'] . '</li>
                    </ul>
              </div>';
    }



}

function loadPlans() {
    if (isset($_POST['Plan-1'])) {
        echo '<!-- Multiple Radios -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="Plan">Plans</label>
                <div class="col-md-4">
                    <div class="radio">
                        <label for="Plan-0">
                            <input type="radio" name="Plan" id="Plan-0" value="Edit/View/Delete Scenarios" onchange="this.form.submit()">
                            Edit/View/Delete All Scenarios
                        </label>
                    </div>
                    <div class="radio">
                        <label for="Plan-1">
                            <input type="radio" name="Plan" id="Plan-1" value="Past Due, Incomplete Scenarios"  checked="checked" onchange="this.form.submit()">
                            Past Due, Incomplete Scenarios
                        </label>
                    </div>
                </div>
            </div>';
    } else {
        echo '<!-- Multiple Radios -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="Plan">Plans</label>
                <div class="col-md-4">
                    <div class="radio">
                        <label for="Plan-0">
                            <input type="radio" name="Plan" id="Plan-0" value="Edit/View/Delete Scenarios" checked="checked" onchange="this.form.submit()">
                            Edit/View/Delete All Scenarios
                        </label>
                    </div>
                    <div class="radio">
                        <label for="Plan-1">
                            <input type="radio" name="Plan" id="Plan-1" value="Past Due, Incomplete Scenarios" onchange="this.form.submit()">
                            Past Due, Incomplete Scenarios
                        </label>
                    </div>
                </div>
            </div>';
    }
}
