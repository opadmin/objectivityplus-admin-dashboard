<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 1/26/2016
 * Time: 12:01 PM
 */

function selectedOrganization() {

    if (isset($_POST['SelectOrganization'])) {
        return $_POST['SelectOrganization'];
    }
    else {
        return 'No selection';
    }

}

function returnOrganizations() {
    try {
        $_host = "db2.apexinnovations.com";
        $connection = new PDO("mysql:host={$_host};dbname=appquantumdb;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');

        /**     Enable exceptions    **/
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $command = $connection->prepare("SELECT name FROM appquantumdb.organization ORDER BY name;");

        if ($command->execute()) {
            $organizations = $command->fetchAll(PDO::FETCH_ASSOC);

            if (!isset($_POST["SelectOrganization"])) {
                echo '<option value="No Selection" selected="selected">No Selection</option>';
            }

            foreach ($organizations as $row) {
                if (isset($_POST["SelectOrganization"]) && $_POST["SelectOrganization"] == $row['name']) {
                    echo '<option value="' . $row['name'] . '" selected="selected">' . $row['name'] . '</option>';
                }
                else {
                    echo '<option value="' . $row['name'] . '">' . $row['name'] . '</option>';
                }
            }
        }

    }
    catch (Exception $e){
        $message = 'Database error. Please try again later';
    }

}
