<?php
require_once 'scripts/php/session.php';
require 'scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="scripts/jquery/links.js"></script>
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="scripts/sweetalert-master/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="scripts/sweetalert-master/dist/sweetalert.css">
    <script src="scripts/socket.io.js" class=""></script>
    <script src="scripts/Cheshire.js" class=""></script>
    <script src="scripts/bootstrap/js/bootstrap.min.js" class=""></script>
    <script src="scripts/javascript/loadOrganizations.js" type="text/javascript"></script>
    <script src="scripts/javascript/conferencing.js" type="text/javascript"></script>
    <script src="scripts/javascript/notifications.js" type="text/javascript"></script>
    <script src="scripts/javascript/bootstrap-notify.js" type="text/javascript"></script>
    <script src="scripts/javascript/bootstrap-notify.min.js" type="text/javascript"></script>
    <script src="scripts/bootstrap-switch-master/dist/js/bootstrap-switch.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="scripts/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.css">

    <script>
        function getMessageText() {
          $.ajax({
              method: "POST",
              url: '/dashboard/OPAPI/index.php',
              dataType:'json',
              data: {
                  controller:'Conferencing',
                  action:'getMessageText',
                  data:{
                      sms : document.getElementById('messageToggle').checked
                  }
              },
              success: function (data) {
                  if(data.success)
                  {
                    $("#message").text('');
                    $("#message").text(data.data);
                  }
                  else
                  {
                      alert(data.errormsg);
                  }
              }
          });
        }

        $(document).ready(function(e) {

            $("#pageToggle").bootstrapSwitch();

            $('#pageToggle').on('switchChange.bootstrapSwitch', function() {

              if ($('#Part3').hasClass("hidden")) {
                $('#Part3_Alt, #Part3line').fadeOut( "fast", function() {
                    $(this).addClass("hidden");
                });
                $('#Part3, #limit').removeClass("hidden").fadeIn( "slow");

              } else {
                $('#Part3, #limit').fadeOut( "fast", function() {
                    $(this).addClass("hidden");
                });
                $('#Part3_Alt, #Part3line').removeClass("hidden").fadeIn( "slow");
              }
            });

            $("#submitDates").on("click", function() {
                getSelected();
            });

            $(function() {
                $("input[name='phone']").keyup(function() {
                    var curchr = this.value.length;
                    var curval = $(this).val();
                    if (curchr == 3) {
                        $("input[name='phone']").val("(" + curval + ")" + " ");
                    } else if (curchr == 9) {
                        $("input[name='phone']").val(curval + "-");
                    }
                });
            });

            $("#messageToggle").bootstrapSwitch();

            $('#messageToggle').on('switchChange.bootstrapSwitch', function (event, state) {
                console.log("state change");
                getMessageText();
            });

            function getSelected() {
                getMessageText();

                $('#insertSelection option').each(function(index){
                    if($(this).is(':selected'))
                    {
                        // selected.push($('#insertSelection option').eq(index).val());
                        (function() {
                          $.ajax({
                              method: "POST",
                              url: '/dashboard/OPAPI/index.php',
                              dataType:'json',
                              data: {
                                  controller:'Conferencing',
                                  action:'getSessions',
                                  data:{
                                      organization : $("#SelectOrganization").find(":selected").val(),
                                      date : ($('#insertSelection option').eq(index).val()),
                                      id : ($('#insertSelection option').eq(index).attr("data-plan"))
                                  }
                              },
                              success: function (data) {
                                  if(data.success)
                                  {

                                      $('#Sessions').append(data.data);
                                  }
                                  else
                                  {
                                      alert(data.errormsg);
                                  }
                              }
                          });
                        })()
                    }
                });
            }
      });

        function loadStartDate() {
          $.ajax({
              method: "POST",
              url: '/dashboard/OPAPI/index.php',
              dataType:'json',
              data: {
                  controller:'Conferencing',
                  action:'getStartDate',
                  data:{
                      organization : $("#SelectOrganization").find(":selected").val()
                  }
              },
              success: function (data) {
                  if(data.success)
                  {
                    // $('#start').val(data.data);
                    $("#start").datepicker({dateFormat: 'yyyy-mm-dd'}).datepicker('setDate', new Date(data.data));
                    $('#dateSpinner').hide();

                    $("#submit").prop("disabled", false).html("Submit");
                  }
                  else
                  {
                      swal("Error", data.errormsg, "error");
                  }
              }
          });
        }
    </script>

    <title>Conference Tool</title>
</head>

<body>

<div id="outerContainer" class="container nopaddingStrict">
    <div id="header_container" class="container navbar">

        <div id="header_banner" class="fullWidth">
            <div class="col-lg-9">
                <!-- <img  class="fullWidth" src="../assets/2013-Objectivity-Plus-Registered.png"> -->
                <img  class="fullWidth" src="../assets/OPBanner.png">
            </div>
            <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
                <img id="Logo" src="assets/Spy-01.png">
            </div>
        </div>
    </div>
    <?php getNavigation(); ?>

    <br>

    <div id="mainContainer" class="col-md-12">
        <div class="row text-center">

            <div id="toggle" class="row text-center">
                <input id="pageToggle" type="checkbox" data-on-text="Schedule" data-off-text="SMS" data-off-color="info" data-size="small" name="my-checkbox" checked>
            </div>

            <div class="col-md-3">
                <div class="input-group" id="instructionsToggleContainer">
                    <span class="input-group-addon">
                        <input id="instructionsToggle" name="instructionsToggle" type="checkbox" aria-label="Checkbox for following text input">
                    </span>
                    <label for="instructionsToggle" class="form-control" aria-label="Toggle instructions">Show Instructions</label>
                </div>
            </div>

        </div>
        <br/>


        <div id="Part1" class="Part">
            <div class="instruction col-lg-6">
                <h3>Instructions: Part 1</h3>
                <ol>
                    <li>Select an organization</li>
                    <li>Select a date range</li>
                    <li>Continue to Part 2</li>
                </ol>
            </div>
            <form class="conference-filter-control form-inline col-lg-6" method="post">
                <fieldset>
                    <!-- Form Name -->
                    <h3>Select Organization & Date Range</h3>

                    <!-- Select Basic -->
                    <div class="">
                        <label class="col-md-4 control-label" for="SelectOrganization">Select Organization</label>
                        <div class="input-group col-md-6">
                            <select id="SelectOrganization" name="SelectOrganization" class="form-control col-md-10" onchange="loadStartDate();">
                            </select>
                            <div id="orgSpinner" class="input-group-addon">
                              <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                            </div>
                        </div>
                    </div>

                    <div class="">
                        <label class="control-label col-md-4" for="start">Start Date:</label>
                        <div class="input-group col-md-6">
                            <input class="form-control" type="text" id="start" name="start">
                            <div id="dateSpinner" class="input-group-addon">
                              <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                            </div>
                        </div>

                    </div>

                    <div class="">
                        <label class="control-label col-md-4" for="end">End Date:&nbsp;</label>
                        <div class="input-group col-md-6">
                            <input class="form-control" type="text" id="end" name="end">
                        </div>
                    </div>

                    <br/>

                    <!-- Button -->
                    <div class="">
                        <label class="col-md-4 control-label" for="singlebutton"></label>
                        <div class="input-group col-md-4">
                            <button id="submit" name="singlebutton" class="btn btn-opblue" disabled>Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <!-- <span id="Part2line" class="sexy_line hidden"></span> -->

        <div id="Part2" class="hidden Part">
            <div class="instruction col-lg-6">
                <h3>Instructions: Part 2</h3>
                'Available Dates' has been populated with every day the selected organization has at least one scenario scheduled
                <ol>
                    <li>Click the dates you wish to start modifying</li>
                    <li>Hint: Hold 'Ctrl' or 'Shift' to select multiple dates or "click, hold, and drag the house" accross your selections</li>
                    <li>Continue to Part 3</li>
                </ol>
            </div>

            <form class="conference-filter-control form-horizontal col-lg-6" method="post">
                <fieldset>
                    <!-- Form Name -->
                    <h3>Select Dates to Modify</h3>

                    <div class="form-group">
                        <label class="control-label col-md-4" for="testBox">Available Dates:</label>
                        <div class="col-md-6">
                            <select multiple id='insertSelection'>
                            </select>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="singlebutton"></label>
                        <div class="col-md-4">
                            <button id="submitDates" name="singlebutton" class="btn btn-opblue">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>

        <!-- <span id="Part3line" class="sexy_line hidden"></span> -->

        <div id="Part3" class="row hidden Part">
            <div class="instruction col-lg-6">
                <h3>Instructions: Part 3</h3>
                The tables below are filled with every date and scenario selected above. Use the tables to manage which
                users and instructors are assigned to each assessment.
                <ol>
                    <li>Use the tabs on the top row to change between available dates</li>
                    <li>Drag and drop users between the bottom container and each column to assign users to different session</li>
                </ol>
            </div>
            <br/>

        <div class="conference-filter-control row">&nbsp;</div>

        <!-- <div id="mainContainer"> -->

            <div id="limit" class="fullWidth hidden text-center">
                <div id="Tabs" class="row btn-group">
                </div>

                <div class="row">&nbsp;</div>

                <div id="ScenarioColumns" class="row">
                    <div id="NavigateLeft" class="NavCol text-center col-md-1 col-centered">
                        <i style="cursor: pointer;" class="fa fa-arrow-circle-left fa-5x" aria-hidden="true"></i>
                    </div>
                    <div id="Scenarios" class="col-md-10">
                      <div id="Scenario0" class="ScenarioCol nopadding col-md-3 col-centered">
                          <div class="ScenarioTitle text-center"></div>
                      </div>
                      <div id="Scenario1" class="ScenarioCol nopadding col-md-3 col-centered">
                          <div class="ScenarioTitle text-center"></div>
                      </div>
                      <div id="Scenario2" class="ScenarioCol nopadding col-md-3 col-centered">
                          <div class="ScenarioTitle text-center"></div>
                      </div>
                      <div id="Scenario3" class="ScenarioCol nopadding col-md-3 col-centered">
                          <div class="ScenarioTitle text-center"></div>
                      </div>
                    </div>
                    <div id="NavigateRight" class="NavCol text-center nopadding col-md-1 col-centered">
                        <i style="cursor: pointer;" class="fa fa-arrow-circle-right fa-5x" aria-hidden="true"></i>
                    </div>
                </div>
                <div id="UsersSubmit" class="row">
                    <div id="Users" class="col-md-10 col-md-offset-1 nopaddingStrict">
                        <div class="col-md-3" id="unwantedUsers">
                            <div style="background: #cacaca; font-weight: bold; height: 48px; line-height: 48px;" class="row text-center">User Holding Area</div>
                            <div id="storedUsers" class="row text-center">
                            </div>
                            <div class="row">
                                <div id="removeDrop" class="text-center">
                                    Remove User From Schedule
                                </div>
                            </div>
                        </div>

                    <div class="col-md-9">

                        <div id="userListHeader" class="row">
                            <div class="col-md-2">
                                <input id="userListShowAllToggle" name="userListShowAllToggle" type="checkbox">
                                <label for="userListShowAllToggle" aria-label="Toggle show all"> Show All</label>
                            </div>
                            <div class="col-md-6">
                                <span style="font-weight: bold">Available Users/<a  id="addNewUserButton" data-toggle="modal" data-target="#userModal">Add New User</span></a>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span id="findUser" class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </span>

                                    <input id="findUserInput" class="form-control" name="search" type="text" placeholder="Search..." aria-label="Search input">
                                </div>
                            </div>
                        </div>

                        <div id="userList" class="row text-center">
                        </div>
                    </div>
                </div>

                </div>
                <!-- <div class="row">
                    <div id="userListFooter" class="col-lg-10 col-lg-offset-1 text-center">
                        <br/>
                        <div class="btn-group">
                            <button id="submit3"  class="btn btn-opblue">Submit All</button>

                            <button id="loadBackup" class="btn btn-opblue">Load Backup</button>
                            <button id="reset" class="btn btn-opblue">Reset Tool</button>
                        </div>
                        <br/>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- </div> -->

        <div id="Part3_Alt" class="row hidden">
            <div id="sessParent" class="col-md-4 col-md-offset-1 lgPadBot">
                <div class="row mdPadTop mdPadBot" style="font-size: 1.4em; font-weight: 700;">&nbsp; Scheduled Sessions &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button type="button" class="btn btn-opblue allButton">Notify All</button>
                </div>

              <div id="Sessions" class="row">

              </div>
            </div>

            <div class="col-md-6 col-md-offset-1">
              <form class="form-horizontal">
                <fieldset>

                <!-- Form Name -->
                <legend>Message</legend>

                <!-- Multiple Checkboxes (inline) -->
                <div class="form-group">
                <label class="col-md-4 control-label" for="smscheck">Email/SMS Toggle</label>
                <div class="col-md-4">
                  <input id="messageToggle" id=""type="checkbox" data-on-text="SMS" data-off-text="Email" data-off-color="info" data-size="small" name="my-checkbox"  checked>
                </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                <label class="col-md-4 control-label" for="message">Message</label>
                <div class="col-md-4">
                <textarea style="margin: 0px -157.538px 0px 0px; width: 322px; height: 180px;" class="form-control" id="message" name="message"></textarea>
                </div>
                </div>

                </fieldset>
                </form>
            </div>
        </div>

        <br />

        <!-- ?php getLowerNav(); ? -->

    </div>

</div>

<!-- Modal -->
<div id="userModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">New User</h4>
  </div>
  <div id="insertModalForm" class="modal-body">
    <form class="form-horizontal">
      <fieldset>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="first_name">First Name</label>
        <div class="col-md-4">
        <input id="first_name" name="first_name" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="middle_name">Middle Name</label>
        <div class="col-md-4">
        <input id="middle_name" name="middle_name" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="last_name">Last Name</label>
        <div class="col-md-4">
        <input id="last_name" name="last_name" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="email">Email</label>
        <div class="col-md-4">
        <input id="email" name="email" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
          <label class="col-md-4 control-label" for="phone">Phone</label>
          <div class="col-md-4">
              <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md">

          </div>
      </div>

      <!-- Select Basic -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="role">Select Role</label>
        <div class="col-md-4">
          <select id="role" name="role" class="form-control">
            <option value="6">Learner</option>
            <option value="7">Adjunct Instructor</option>
            <option value="22">Lead Instructor</option>
            <option value="9">Admin</option>
          </select>
        </div>
      </div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="submitNewUser"></label>
        <div class="col-md-4">
          <button id="submitNewUser" name="submitNewUser" class="btn btn-opblue">Submit</button>
        </div>
      </div>

      </fieldset>
      </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>

</div>
</div>

<!-- Load Backup Modal -->
<div id="backupModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Load Backup</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    Select a Backup to load:
                </div>
                <div class="row">
                    <div id="backups" class="col-md-8 col-md-offset-2">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<!-- Socket Group Modal -->
<div id="socketModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Join Group</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="socketForm" class="col-md-8 col-md-offset-2">
                        <form class="form-horizontal">
                            <fieldset>
                                <!-- Form Name -->
                                <legend>Pick Name</legend>
                                <div class="form-group">
                                    <label for="socketUserName" class="control-label col-sm-2">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="socketUserName" placeholder="Enter the name the group will see" required="">
                                    </div>
                                </div>

                            </fieldset>
                        </form>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button id="GroupUser" type="button" class="btn btn-opblue" data-dismiss="modal">Enter Group</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Single User</button>
            </div>
        </div>

    </div>
</div>

<!-- User Search Modal -->
<div id="searchModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Join Group</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="searchForm" class="col-md-8 col-md-offset-2">
                        <form class="form-horizontal">
                            <fieldset>

                                <!-- Form Name -->
                                <legend>Locate a Student</legend>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="first_name">First Name</label>
                                    <div class="col-md-4">
                                        <input id="first_name1" name="first_name" type="text" placeholder="John" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="last_name">Last name</label>
                                    <div class="col-md-4">
                                        <input id="last_name1" name="last_name" type="text" placeholder="Doe" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="search"></label>
                                    <div class="col-md-4">
                                        <button id="search" name="search" class="btn btn-primary">Search</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button id="GroupUser" type="button" class="btn btn-opblue" data-dismiss="modal">Enter Group</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Single User</button>
            </div>
        </div>

    </div>
</div>

<!-- User Search Modal -->
<div id="searchModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Join Group</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="searchForm" class="col-md-8 col-md-offset-2">
                      <form class="form-horizontal">
                          <fieldset>

                          <!-- Form Name -->
                          <legend>New Session</legend>

                          <!-- Text input-->
                          <div class="form-group">
                          <label class="col-md-4 control-label" for="newJudgingPlan">Judging Plan</label>
                          <div class="col-md-4">
                          <input id="newJudgingPlan" name="newJudgingPlan" type="text" placeholder="" class="form-control input-md">

                          </div>
                          </div>

                          <!-- Prepended text-->
                          <div class="form-group">
                          <label class="col-md-4 control-label" for="prependedtext">Start Time</label>
                          <div class="col-md-4">
                            <div class="input-group">
                              <span class="input-group-addon">prepend</span>
                              <input id="prependedtext" name="prependedtext" class="form-control" placeholder="HH:MM  in UTC" type="text">
                            </div>

                          </div>
                          </div>

                          <!-- Prepended text-->
                          <div class="form-group">
                          <label class="col-md-4 control-label" for="endTime">End Time</label>
                          <div class="col-md-4">
                            <div class="input-group">
                              <span class="input-group-addon">prepend</span>
                              <input id="endTime" name="endTime" class="form-control" placeholder="HH:MM in UTC" type="text">
                            </div>

                          </div>
                          </div>

                          <!-- Text input-->
                          <div class="form-group">
                          <label class="col-md-4 control-label" for="location">Location</label>
                          <div class="col-md-4">
                          <input id="location" name="location" type="text" placeholder="" class="form-control input-md">

                          </div>
                          </div>

                          <!-- Select Basic -->
                          <div class="form-group">
                          <label class="col-md-4 control-label" for="status">Status</label>
                          <div class="col-md-4">
                            <select id="status" name="status" class="form-control">
                              <option value="Paid">Paid</option>
                              <option value="Cancel">Cancel</option>
                              <option value="Needs Payment">Needs Payment</option>
                            </select>
                          </div>
                          </div>

                          <!-- Button -->
                          <div class="form-group">
                            <label class="col-md-4 control-label" for="submitNewSession"></label>
                            <div class="col-md-4">
                              <button id="submitNewSession" name="submitNewSession" class="btn btn-primary">Submit</button>
                            </div>
                          </div>

                          </fieldset>
                          </form>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button id="GroupUser" type="button" class="btn btn-opblue" data-dismiss="modal">Enter Group</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Single User</button>
            </div>
        </div>

    </div>
</div>

</body>
</html>
