<?php
require_once 'scripts/php/session.php';
include 'scripts/php/organizations.php';
include 'scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="scripts/socket.io.js" class=""></script>
    <script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="scripts/Cheshire.js" class=""></script>
    <script src="scripts/bootstrap/js/bootstrap.min.js" class=""></script>
    <script src="scripts/javascript/ticketing.js" type="text/javascript"></script>

    <title>Ticketing System</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img  class="fullWidth" src="../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>
    <br>

    <form class="form-horizontal">
        <fieldset>

            <!-- Form Name -->
            <legend>Select Option</legend>

            <!-- Multiple Radios -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="radios">Choose an Option</label>
                <div class="col-md-4">
                    <div class="radio">
                        <label for="radios-0">
                            <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
                            New Ticket
                        </label>
                    </div>
                    <div class="radio">
                        <label for="radios-1">
                            <input type="radio" name="radios" id="radios-1" value="2">
                            View/Edit Existing Ticket
                        </label>
                    </div>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="submit"></label>
                <div class="col-md-4">
                    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </fieldset>
    </form>

    <div id="form2">

    </div>

    <div id="comments">

    </div>


    <div class="row">
        <div class="fullWidth OPOrange text-center whiteText smPadBot smPadTop">
            &nbsp;
        </div>
    </div>

    <div id="alterForm">
    </div>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

    <?php getLowerNav(); ?>

</div>

</body>
</html>
