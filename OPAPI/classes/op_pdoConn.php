<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 4/11/2016
 * Time: 11:00 AM
 */
class op_pdoConn {

    protected static $dbPDO;

    private function __construct($production = null){

        if(isset($_SERVER['HTTP_HOST']) &&  $_SERVER['HTTP_HOST'] == 'devbox2.apexinnovations.com' || isset($production) && $production)
        {
//            $host = 'devbox2.apexinnovations.com';
            $host = 'db2.apexinnovations.com';
            // error_log('DB: devbox');
        } else {
            $host = 'db2.apexinnovations.com';
            // error_log('host:' .$_SERVER['HTTP_HOST']);
        }

        try{
            self::$dbPDO = new PDO("mysql:host={$host};dbname=appquantumdb;", 'opadmin', 'A5rEwombo2i4wWekdfrF', array(
                PDO::ATTR_PERSISTENT => false));
        }
        catch(PDOException $e){
            throw new Exception("Connection Error: " . $e->getMessage());
        }
    }

    public static function getConnection($production = null){
        if(!self::$dbPDO){
            new op_pdoConn($production);
        }

        return self::$dbPDO;
    }
}
