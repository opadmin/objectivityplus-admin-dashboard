<?php
// Define path to data folder
//error_reporting(E_ALL);
session_start();

$user = null;
require_once (__DIR__.'/vendor/autoload.php');
header("Content-Type: application/json", true);
if(isset($_SESSION['employee_id']))
{
    handleAdminSession();
}

function handleAdminSession()
{
    global $user;
    $user = $_SESSION['employee_id'];
}

function validUser()
{
    global $user;
    return $user != null;
}

if(validUser())
{
    //wrap the whole thing in a try-catch block to catch any wayward exceptions!
    try {
        //get all of the parameters in the POST/GET request
        $params = $_REQUEST;

        //get the controller and format it correctly so the first
        //letter is always capitalized
        $controller = ucfirst(strtolower($params['controller']));

        //get the action and format it correctly so all the
        //letters are not capitalized, and append 'Action'
        $action = strtolower($params['action']) . 'Action';

        //check if the controller exists. if not, throw an exception
        if (file_exists("controllers/{$controller}.php")) {
            include_once "controllers/{$controller}.php";
        } else {
            throw new Exception('Controller is invalid.');
        }

        //create a new instance of the controller, and pass
        //it the parameters from the request
        $controller = new $controller($params,$user);

        //check if the action exists in the controller. if not, throw an exception.
        if (method_exists($controller, $action) === false) {
            throw new Exception('Action is invalid.');
        }
//        error_log('1');

        //execute the action
        $result['data'] = $controller->$action();
        $result['success'] = true;

    } catch (Exception $e) {
        //catch any exceptions and report the problem
        $result = array();
        $result['success'] = false;
        $result['errormsg'] = $e->getMessage();
        error_log($e->getMessage());
    }
}
else
{
//    error_log('3');
    $params = $_REQUEST;
    $controller = ucfirst(strtolower($params['controller']));
    if ($controller == "Reset") {
        //get the action and format it correctly so all the
        //letters are not capitalized, and append 'Action'
        $action = strtolower($params['action']) . 'Action';

        //check if the controller exists. if not, throw an exception
        if (file_exists("controllers/{$controller}.php")) {
            include_once "controllers/{$controller}.php";
        } else {
            throw new Exception('Controller is invalid.');
        }

        //create a new instance of the controller, and pass
        //it the parameters from the request
        $controller = new $controller($params,$user);

        //check if the action exists in the controller. if not, throw an exception.
        if (method_exists($controller, $action) === false) {
            throw new Exception('Action is invalid.');
        }
//        error_log('1');

        //execute the action
        $result['data'] = $controller->$action();
        $result['success'] = true;
    } else {
        error_log("--------DERO---------");
        error_log(ucfirst(strtolower($params['controller'])));
        $result = array();
        $result['success'] = false;
        $result['errormsg'] = "Invalid user. Try logging on again. User ID: " . $user;
    }

}
// error_log(print_r($result,1));
//echo the result of the API call
echo json_encode($result);
exit();