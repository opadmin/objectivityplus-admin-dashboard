<?php
use \ForceUTF8\Encoding;

class Ticketing
{
    private $_params;
    private $data;
    private $pdo;
    private $user;

    public function __construct($params,$user)
    {
        $this->_params = $params;
        $this->data = !isset($params['data']) ?: $params['data'];
        $this->user = $user;
        //Open database connection
        $this->pdo = op_pdoConn::getConnection();
    }

    public function newTicketFormAction() {

        return '<form class="form-horizontal">
                    <fieldset>
                    
                    <!-- Form Name -->
                    <legend>New Ticket</legend>
                    
                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="user_name">Name of User with Issue</label>  
                      <div class="col-md-4">
                      <input id="user_name" name="user_name" type="text" placeholder="ex. \'Jane Doe\'" class="form-control input-md" required="">
                        
                      </div>
                    </div>
                    
                    <!-- Text input-->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="email">User\'s Email </label>  
                      <div class="col-md-4">
                      <input id="email" name="email" type="text" placeholder="ex. \'janedoe@something.com\'" class="form-control input-md" required="">
                        
                      </div>
                    </div>
                    
                    <!-- Textarea -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="message">Explain the Issue</label>
                      <div class="col-md-4">                     
                        <textarea class="form-control" id="message" name="message">Dates, descriptions, is it recurring, etc.</textarea>
                      </div>
                    </div>
                    
                    <!-- Button -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for="submit2"></label>
                      <div class="col-md-4">
                        <button id="submit2" name="submit2" class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                    
                    </fieldset>
                    </form>';
    }

    public function newTicketAction() {
        $username = $this->data['username'];
        $email = $this->data['email'];
        $message = $this->data['message'];

        $command = $this->pdo->prepare("INSERT INTO Quantum.tickets (user_name, user_email, message, open_date, status) VALUES (:name, :email, :message, NOW(), 'OPEN');");

        if ($command->execute(array(":email"=>$email, ":name"=>$username, ":message"=>$message))) {
            return "success";
        } else {
            throw new exception("Insert failed");
        }
    }

    public function getTicketsAction() {
        
        $connection = $this->pdo->prepare('SELECT id, status, open_date, user_name, message FROM Quantum.tickets ORDER BY open_date DESC;');

        if ($connection->execute()) {
            $tickets = $connection->fetchAll(PDO::FETCH_ASSOC);

            $result = '<div class="row">';

            foreach($tickets as $row) {
                $result .=  '<div id="' . $row['id'] . '" class="col-lg-12 ticket text-center ' . $row['status'] . '">';
                $result .= '<h3>' .  $row['user_name'] . '</h3>';
                $result .= '<ul style="list-style:none;"><li><strong>Opened On: </strong>' . $row['open_date'] . ' CST</li><li><strong>Status: </strong>' . strtoupper($row['status']) . '</li><li>' . $row['message'] . '</li></ul><br/>';
                $result .= '</div>';
            }

            return $result;
        } else {
            throw new exception("Cannot pull tickets.");
        }
    }

    public function editTicketFormAction() {
        $id = $this->data['id'];

        $connection = $this->pdo->prepare('SELECT id, status, open_date, closed_date, user_email, user_name, closed_by, cause, message FROM Quantum.tickets WHERE id = :id;');

        if ($connection->execute(array(":id"=>$id))) {
            $ticket = $connection->fetch(PDO::FETCH_ASSOC);

            $result = '<form class="form-horizontal">
                            <fieldset>
                            
                            <!-- Form Name -->
                            <legend>Edit Ticket</legend>
                            
                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="id">Ticket ID</label>  
                              <div class="col-md-2">
                              <input id="id" readonly name="id" type="text" value="' . $ticket['id'] . '" class="form-control input-md">
                                
                              </div>
                            </div>
                            
                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="username">User\'s Name</label>  
                              <div class="col-md-4">
                              <input id="username" name="username" type="text" value="' . $ticket['user_name'] . '" class="form-control input-md">
                                
                              </div>
                            </div>
                            
                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="email">User\'s Email</label>  
                              <div class="col-md-4">
                              <input id="email" name="email" type="text" value="' . $ticket['user_email'] . '" class="form-control input-md">
                                
                              </div>
                            </div>
                            
                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="opendate">Date Opened</label>  
                              <div class="col-md-4">
                              <input id="opendate" name="opendate" type="text" value="' . $ticket['open_date'] . '" class="form-control input-md">
                                
                              </div>
                            </div>
                            
                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="close_date">Date Closed</label>  
                              <div class="col-md-4">
                              <input id="close_date" name="close_date" type="text" value="' . $ticket['closed_date'] . '" class="form-control input-md">
                                
                              </div>
                            </div>';

                            if (strtoupper($ticket['status']) == 'OPEN') {
                                $result .= '<!-- Select Basic -->
                                    <div class="form-group">
                                      <label class="col-md-4 control-label" for="status">Status</label>
                                      <div class="col-md-4">
                                        <select id="status" name="status" class="form-control">
                                          <option selected value="OPEN">OPEN</option>
                                          <option value="CLOSED">CLOSED</option>
                                        </select>
                                      </div>
                                    </div>';
                            } else {
                                $result .= '<!-- Select Basic -->
                                    <div class="form-group">
                                      <label class="col-md-4 control-label" for="status">Status</label>
                                      <div class="col-md-4">
                                        <select id="status" name="status" class="form-control">
                                          <option value="OPEN">OPEN</option>
                                          <option selected value="CLOSED">CLOSED</option>
                                        </select>
                                      </div>
                                    </div>';
                            }

                            
                            $result .= '<!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="solved_by">Solved By</label>  
                              <div class="col-md-4">
                              <input id="solved_by" name="solved_by" value="' . $ticket['closed_by'] . '" type="text" class="form-control input-md">
                                
                              </div>
                            </div>
                            
                            <!-- Select Basic -->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="cause">Cause of Issue</label>
                              <div class="col-md-4">
                                <select id="cause" name="cause" class="form-control">
                                ' ; 

                                $options = ["Unknown","User Error","Tablet Bug","Website Bug","Complete System Bug","New Development","Cosmetic Issue","Unrelated to OP"];
                                foreach($options as $option)
                                {
                                  $result .= '<option value="' . $option . '" ' . ($option == $ticket['cause'] ? 'selected' : '') . '>' . $option . '</option>';
                                }
                                  
                                  
                                $result .= '
                                </select>
                              </div>
                            </div>
                            <!-- Textarea -->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="message">Message</label>
                              <div class="col-md-4">                     
                                <textarea class="form-control" id="message" name="message">' . $ticket['message'] . '</textarea>
                              </div>
                            </div>
                            
                            <!-- Button -->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="submitEdit"></label>
                              <div class="col-md-4">
                                <button id="submitEdit" name="submitEdit" class="btn btn-primary">Submit Edits</button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button id="delete" name="delete" class="btn btn-warning">Delete Ticket</button>
                              </div>
                            </div>
                            
                            </fieldset>
                            </form>
                            ';

            return $result;
        } else {
            throw new exception("Couldn't generate form");
        }
    }

    public function editTicketAction() {

        $connection = $this->pdo->prepare("UPDATE Quantum.tickets 
                                                  SET user_name = :username,
                                                      user_email = :email,
                                                      status = :status,
                                                      open_date = :open_date,
                                                      closed_date = :closed_date,
                                                      closed_by = :solved_by,
                                                      cause = :cause,
                                                      message = :message 
                                                  WHERE id = :id;");


        if ($connection->execute(array(
                                        ":username"=>$this->data['username'],
                                        ":email"=>$this->data['email'],
                                        ":status"=>$this->data['status'],
                                        ":solved_by"=>$this->data['solved_by'],
                                        ":cause"=>$this->data['cause'],
                                        ":open_date"=>$this->data['open_date'],
                                        ":closed_date"=>$this->data['closed_date'],
                                        ":message"=>$this->data['message'],
                                        ":id"=>$this->data['id']
                                        ))) {
            return "success";
        } else {
            throw new exception("Failed to update");
        }
    }

    public function deleteTicketAction() {

        $connection = $this->pdo->prepare("DELETE
                                              FROM Quantum.tickets
                                              WHERE id = :id;");


        if ($connection->execute(array(":id"=>$this->data['id']))) {
            return "success";
        } else {
            throw new exception("Failed to delete");
        }
    }

    public function getCommentsAction() {
        $connection = $this->pdo->prepare('SELECT emp.id, CONCAT(`last_name`, \', \', `first_name`) AS name, comment, created_date, icon FROM Quantum.comments com INNER JOIN Quantum.Employees emp ON com.user_id = emp.id WHERE ticket_id = :id ORDER BY created_date;');

        if ($connection->execute(array(':id'=>$this->data['id']))) {
            $comments = $connection->fetchAll(PDO::FETCH_ASSOC);
             $result = '';

            if (!$comments) {
                $result = '<h3>No Comments Found...</h3>';
            } else {
                $result .= '<legend><strong>Comments</strong></legend>';
                foreach( $comments as $row) {
                    $result .= '<div class="row comment mdPadTop mdPadBot"><div class="col-lg-1"><img class="img-responsive picture" src="' . $row['icon'] . '"></div><div class="col-lg-2">' . $row['name'] . '<br/>' . $row['created_date'] .'</div><div class="commentText col-lg-8">' .
                        $row['comment'] . '</div></div>';
                }
            }

            $result .= '<br/><div class="form-group">
                          <div class="col-md-offset-4 col-md-4">
                            <div class="input-group">
                              <input id="comment" name="buttondropdown" class="form-control" placeholder="Type New Comment Here" type="text">
                              <div class="input-group-btn">
                                <button id="newComment" type="button" class="btn btn-default">
                                  Submit
                                </button>
                              </div>
                            </div>
                          </div>
                        </div> 
                        <br/><br/>';

            return $result;
        } else {
            throw new exception("Cannot load comments");
        }
    }
    
    public function submitCommentAction() {

        $connection = $this->pdo->prepare('INSERT INTO Quantum.comments (user_id, ticket_id, comment, created_date) VALUES (:user, :id, :comment, NOW());');

        if ($connection->execute(array(":user"=>($this->user), ":id"=>$this->data['id'], "comment"=>$this->data['comment']))) {
            return "Success";
        } else {
            throw new exception("Unable to add comment");
        }
        
    }

}