<?php
use \ForceUTF8\Encoding;

class Manager
{
    private $_params;
    private $data;
    private $pdo;
    private $user;

    public function __construct($params,$user)
    {
        $this->_params = $params;
        $this->data = !isset($params['data']) ?: $params['data'];
        $this->user = $user;
        //Open database connection
        $this->pdo = op_pdoConn::getConnection();
    }

    public function pullQuestionsAction() {
        $query = $this->pdo->prepare('SELECT id, Question
                                        FROM Quantum.Website
                                        WHERE Category = \'FAQ\' OR Category = \'Data\'
                                        ORDER BY OrderID;');
        if ($query->execute()) {
            $results = $query->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } else {
            throw new exception('Unable to fetch FAQs.');
        }


    }

    public function pullTeamAction() {
        $query = $this->pdo->prepare('SELECT Question
                                    FROM Quantum.Website
                                    WHERE Category = \'BioHeader\'
                                    ORDER BY OrderID;');
        $query->execute();
        if ($query->execute()) {
            $results = $query->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } else {
            throw new exception('Unable to fetch Team members.');
        }


    }

    public function generateFAQFormAction() {

        $user = $this->data['selection'];

        $query = $this->pdo->prepare('SELECT Question, Answer, id
                                        FROM Quantum.Website
                                        WHERE (Category = \'FAQ\' OR Category = \'Data\')
                                        AND Question = :user;');
        $query->execute(array(":user"=>$user));

        $faqs = $query->fetch(PDO::FETCH_ASSOC);

        $result = ['form' => '<div class="row">
                            <div class="fullWidth OPOrange text-center whiteText smPadBot smPadTop">
                                <h4>Edit the FAQ:</h4>
                            </div>
                        </div>
                        <form class="form-horizontal" action="../scripts/php/updateFAQ.php" method="post">
            <fieldset>

            <!-- Form Name -->
            <legend>Edit FAQ</legend>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Question">Question</label>
              <div class="col-md-4">
              <input id="Question" name="Question" type="text" placeholder="Question" value="' . $faqs['Question'] . '" class="form-control input-md" required="">

              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Question">Question ID</label>
              <div class="col-md-4">
              <input id="ID" name="ID" type="text" placeholder="ID" value="' . $faqs['id'] . '" class="form-control input-md" readonly>

              </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Answer">Answer</label>
              <div class="col-md-4">
                <textarea class="form-control" id="Answer" name="Answer">"' . $faqs['Answer'] . '"</textarea>
              </div>
            </div>

            <!-- Button -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="submit"></label>
              <div class="col-md-4">
                <button id="submit" name="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>

            </fieldset>
            </form>'];

        return $result;
    }

    public function generateBioFormAction() {
        $user = $this->data['selection'];

        $query = $this->pdo->prepare('SELECT Question, Answer, Department, Category, orderID, id
                                        FROM Quantum.Website
                                        WHERE (Category = \'Bio\')
                                        AND Question = :user
                                        ORDER BY OrderID;');
        $query->execute(array(":user"=>$user));

        $bio = $query->fetch(PDO::FETCH_ASSOC);

        $query = $this->pdo->prepare('SELECT Question, Answer, Department, Category, orderID, id
                                        FROM Quantum.Website
                                        WHERE (Category = \'BioHeader\')
                                        AND Question = :user
                                        ORDER BY OrderID;');
        $query->execute(array(":user"=>$user));

        $bioHeader = $query->fetch(PDO::FETCH_ASSOC);

        $query = $this->pdo->prepare('SELECT Question, Answer, Department, Category, orderID, id
                                        FROM Quantum.Website
                                        WHERE (Category = \'BioImg\')
                                        AND Question = :user
                                        ORDER BY OrderID;');
        $query->execute(array(":user"=>$user));

        $img = $query->fetch(PDO::FETCH_ASSOC);

        $result = ['form' => '<div class="row">
                            <div class="fullWidth OPOrange text-center whiteText smPadBot smPadTop">
                                <h4>Edit the Bio:</h4>
                            </div>
                        </div>
            <form class="form-horizontal" action="../scripts/php/updateBio.php" method="post">
            <fieldset>

            <!-- Form Name -->
            <legend>Edit Bio</legend>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="BioHeader">Header</label>
              <div class="col-md-4">
              <input id="BioHeader" name="BioHeader" type="text" placeholder="name and title" value="' . $bioHeader['Answer'] . '" class="form-control input-md" required="">

              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Question">Bio ID</label>
              <div class="col-md-4">
              <input id="headerID" name="headerID" type="text" placeholder="headerID" value="' . $bioHeader['id'] . '" class="form-control input-md" readonly>

              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Question">Img ID</label>
              <div class="col-md-4">
              <input id="imgID" name="imgID" type="text" placeholder="imgID" value="' . $img['id'] . '" class="form-control input-md" readonly>

              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Question">Header ID</label>
              <div class="col-md-4">
              <input id="bioID" name="bioID" type="text" placeholder="bioID" value="' . $bio['id'] . '" class="form-control input-md" readonly>

              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Question">Image</label>
              <div class="col-md-4">
              <input id="img" name="img" type="text" placeholder="image" value="' . $img['Answer'] . '" class="form-control input-md">

              </div>
            </div>



            <!-- Textarea -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Bio">Biography</label>
              <div class="col-md-4">
                <textarea class="form-control" id="Bio" name="Bio">"' . $bio['Answer'] . '"</textarea>
              </div>
            </div>

            <!-- Button -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="submit"></label>
              <div class="col-md-4">
                <button id="submit" name="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>

            </fieldset>
            </form>'];

        return $result;
    }

    public function pullScenariosAction() {

            $org = $this->data["organization"];
            $start = $this->data["startDate"];
            $end = $this->data["endDate"];
            $conf = isset($this->data["conf"]);


            if ($conf) {
                $command = $this->pdo->prepare('SELECT sess.id, sess.start_datetime, sess.status, scen.name as ScenName
                                            FROM appquantumdb.judging_plan_session				sess
                                                INNER JOIN appquantumdb.judging_plan				plan
                                                    ON	sess.judging_plan_id = plan.id
                                                INNER JOIN appquantumdb.organization				org
                                                    ON	plan.organization_id = org.id
                                                INNER JOIN appquantumdb.scenario					scen
                                                    ON	plan.scenario_id = scen.id
                                            WHERE org.name = :organization
                                            AND start_datetime > :date1
                                            AND	end_datetime < :date2
                                            and plan.status != "cancel";');

                $command->execute(array(":organization"=>$org, ":date1"=>$start, ":date2"=>$end));

                $scenarios = $command->fetchAll(PDO::FETCH_ASSOC);

                $result = '';

                foreach ($scenarios as $row) {
                    $id = $row['id'];
                    $startDate = $row['start_datetime'];
                    $name = $row['ScenName'];

                    $result .= '<option value="' . $id . '">' . $name . ', ' . $startDate . '</option>';
                }

                return $result;
            } else {
                $command = $this->pdo->prepare('SELECT sess.id, sess.start_datetime, sess.status, sess.end_datetime, plan.courseTitle, scen.name as ScenName, org.name
                                            FROM appquantumdb.judging_plan_session				sess
                                                INNER JOIN appquantumdb.judging_plan				plan
                                                    ON	sess.judging_plan_id = plan.id
                                                INNER JOIN appquantumdb.organization				org
                                                    ON	plan.organization_id = org.id
                                                INNER JOIN appquantumdb.scenario					scen
                                                    ON	plan.scenario_id = scen.id
                                            WHERE org.name = :organization
                                            AND start_datetime > :date1
                                            AND	end_datetime < :date2;');

                $command->execute(array(":organization"=>$org, ":date1"=>$start, ":date2"=>$end));

                $scenarios = $command->fetchAll(PDO::FETCH_ASSOC);

                $result = '';

                foreach ($scenarios as $row) {
                    $result .= '<div class="ScenarioBlock col-md-offset-0 col-sm-offset-1 col-xs-offset-1 col-md-3 col-sm-4 col-xs-9 text-left" id="' . $row['id'] . '">
                    <h4>' . $name . '</h4>
                    <ul style="list-style-type: none">
                        <li><strong>Session ID</strong>: ' . $row['id'] . '</li>
                        <li><strong>Organization</strong>: ' . $row['name'] . '</li>
                        <li><strong>Scenario</strong>: ' . $row['ScenName'] . '</li>
                        <li><strong>Start Date</strong>: ' . $row['start_datetime'] . '</li>
                        <li><strong>End Date</strong>: ' . $row['end_datetime'] . '</li>
                        <li><strong>Status</strong>: ' . $row['status'] . '</li>
                    </ul>
                    <div class="btn-group btn-group-justified mdPadBot">
                        <a type="edit" data-toggle="modal" data-target="#editModal" class="edit EditBtn btn btn-default">Edit</a>
                        <a type="submit" class="DeleteBtn btn btn-default">Delete</a>
                    </div>
              </div>';
                }

                return $result;
            }

    }

    public function pullIncompleteScenariosAction() {

        $query = 'SELECT org.name, sess.id, sess.end_datetime, sess.status, sess.invoice_id
                FROM 	  appquantumdb.judging_plan_session					sess
                INNER JOIN appquantumdb.judging_plan						jud
                    ON sess.judging_plan_id = jud.id
                INNER JOIN appquantumdb.organization						org
                    ON	jud.organization_id = org.id
                LEFT JOIN appquantumdb.judging_plan_session_result			res
                    ON	  sess.id = res.session_id
                WHERE res.session_id IS NULL
                AND org.name != \'Test School\'
                AND jud.status != \'cancel\'';


        if($this->data['unknown']) {
            $query = $query . ' AND  sess.status IS NULL ';
        } elseif ($this->data['paid']) {
            $query = $query . ' AND  sess.status = \'Paid\' ';
        } elseif ($this->data['needsPayment']) {
            $query = $query . ' AND  sess.status = \'Needs Payment\' ';
        } elseif ($this->data['cancelled']) {
            $query = $query . ' AND  sess.status = \'Cancelled\' ';
        }

        $query = $query . ' ORDER BY sess.end_datetime DESC;';

        $command = $this->pdo->prepare($query);
        $command->execute();
        $pastScenarios = $command->fetchAll(PDO::FETCH_ASSOC);

        $result = '';

        foreach ($pastScenarios as $row) {
            $result .= '<div class="ScenarioBlock col-md-offset-0 col-sm-offset-1 col-xs-offset-1 col-md-3 col-sm-4 col-xs-9 text-left" id="' . $row['id'] . '">
                    <h4>' . $row['name'] . '</h4>
                    <ul style="list-style-type: none">
                        <li><strong>End Date</strong>: ' . $row['end_datetime'] . '</li>
                        <li><strong>Organization</strong>: ' . $row['name'] . '</li>
                        <li><strong>Status</strong>: ' . $row['status'] . '</li>
                        <li><strong>Session ID</strong>: ' . $row['id'] . '</li>
                        <li><strong>Invoice ID</strong>: ' . $row['invoice_id'] . '</li>
                    </ul>
              </div>';
        }

        return $result;

    }

    public function pullCreditsAction() {
        $org = $this->data["SelectOrganization"];
        $start = $this->data["start"];
        $end = $this->data["end"];

        $command = $this->pdo->prepare('SELECT sess.id, sess.start_datetime, sess.end_datetime, scen.name
                                            FROM appquantumdb.judging_plan_session				sess
                                                INNER JOIN appquantumdb.judging_plan				plan
                                                    ON	sess.judging_plan_id = plan.id
                                                INNER JOIN appquantumdb.organization				org
                                                    ON	plan.organization_id = org.id
                                                INNER JOIN appquantumdb.scenario					scen
                                                    ON	plan.scenario_id = scen.id
                                            WHERE org.name = :organization
                                            AND jud.status != \'cancel\'
                                            AND start_datetime > :date1
                                            AND	end_datetime < :date2;');

        $command->execute(array(":organization"=>$org, ":date1"=>$start, ":date2"=>$end));

        $scenarios = $command->fetchAll(PDO::FETCH_ASSOC);

        $result = '';

        foreach ($scenarios as $row) {
            $id = $row['id'];
            $name = $row['name'];
            $startDate = $row['start_datetime'];
            $endDate = $row['end_datetime'];
            $scenario = $row['name'];


            $result .= '<div class="ScenarioBlock col-md-offset-0 col-sm-offset-1 col-xs-offset-1 col-md-3 col-sm-4 col-xs-9 text-left">
                    <h4>' . $name . '</h4>
                    <ul style="list-style-type: none">
                        <li><strong>Start Date</strong>: ' . $startDate . '</li>
                        <li><strong>End Date</strong>: ' . $endDate . '</li>
                        <li><strong>Organization</strong>: ' . $name . '</li>
                        <li><strong>Scenario</strong>: ' . $scenario . '</li>
                        <li><strong>Session ID</strong>: ' . $id . '</li>
                    </ul>
                    <div class="btn-group btn-group-justified mdPadBot">
                        <a type="submit" class="PaidBtn btn btn-default">Set <br/> Paid</a>
                        <a type="submit" class="UnPaidBtn btn btn-default">Set <br/> Unpaid</a>
                    </div>
              </div>';
        }
    }

    public function pullScenarioDaysAction() {

        $org = $this->data["organization"];
        $start = $this->data["startDate"];
        $end = $this->data["endDate"];

        $command = $this->pdo->prepare('SELECT DISTINCT(DATE_FORMAT(sess.start_datetime,\'%m/%d/%Y\')) as Dates
                                            FROM appquantumdb.judging_plan_session				sess
                                                INNER JOIN appquantumdb.judging_plan				plan
                                                    ON	sess.judging_plan_id = plan.id
                                                INNER JOIN appquantumdb.organization				org
                                                    ON	plan.organization_id = org.id
                                                INNER JOIN appquantumdb.scenario					scen
                                                    ON	plan.scenario_id = scen.id
                                            WHERE org.name = :organization
                                            AND jud.status != \'cancel\'
                                            AND start_datetime > :date1
                                            AND	end_datetime < :date2;');

        $command->execute(array(":organization"=>$org, ":date1"=>$start, ":date2"=>$end));

        $scenarios = $command->fetchAll(PDO::FETCH_ASSOC);

        $result = '';

        foreach ($scenarios as $row) {
            $result .= '<option value="' . $row['Dates'] . '">' . $row['Dates'] . '</option>';
        }

        return $result;
    }

    public function pullJudgingPlansAction() {

        $query = 'SELECT jud.id, courseTitle, courseNumber, status, created_datetime
                                            FROM appquantumdb.judging_plan jud
                                            INNER JOIN appquantumdb.user_main usr
                                            ON usr.id = jud.created_user_id
                                            INNER JOIN appquantumdb.organization org
                                              ON jud.organization_id = org.id
                                            WHERE org.name = :organization';

        if ($this->data['create'] == 'true') {
            $query .= ' AND status != "create"';
        }
        if ($this->data['approve'] == 'true') {
            $query .= ' AND status != "approve"';
        }
        if ($this->data['runningScoring'] == 'true') {
            $query .= ' AND status != "runningScoring"';
        }
        if ($this->data['complete'] == 'true') {
            $query .= ' AND status != "complete"';
        }
        if ($this->data['cancel'] == 'true') {
            $query .= ' AND status != "cancel"';
        }

        $query .= ';';

        $command = $this->pdo->prepare($query);

        if($command->execute(array(":organization"=>$this->data['organization']))) {
            $plan = $command->fetchAll(PDO::FETCH_ASSOC);
            $result = '<label class="col-md-4 control-label" for="selection">Select a Judging Plan</label>
                <div class="col-md-4">
                <select id="selection" class="form-control">';

            foreach($plan as $row) {
                $result .= '<option value="' . $row['id'] . '">' . $row['id'] . ', ' . $row['courseTitle'] . ', ' . $row['courseNumber'] . ', ' . $row['status'] . ', ' . $row['created_datetime'] . '</option>';
            }

            $result .= '</select></div>';

            return $result;
        } else {
            throw new exception("Could not query database");
        }
    }

    public function judgingPlanEditFormAction() {
        $command = $this->pdo->prepare("SELECT jud.id as id, status, courseTitle, courseNumber, created_user_id, CONCAT(`last_name`, ', ', `first_name`) AS Director
                                             FROM appquantumdb.judging_plan jud
                                             INNER JOIN appquantumdb.user_main usr
                                                ON jud.created_user_id = usr.id
                                             WHERE jud.id = :id");

        if($command->execute(array(":id"=>$this->data['id']))) {
            $plan = $command->fetch(PDO::FETCH_ASSOC);

            $result = '<form class="form-horizontal">
                            <fieldset>

                            <!-- Form Name -->
                            <legend>Judging Plan Details</legend>

                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="id">Judging Plan ID</label>
                              <div class="col-md-4">
                              <input id="id" name="id" type="text" readonly placeholder="" value="' . $plan['id'] . '" class="form-control input-md">

                              </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="title">Course Title</label>
                              <div class="col-md-4">
                              <input id="title" name="title" type="text" placeholder="" value="' . $plan['courseTitle'] . '" class="form-control input-md">

                              </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="number">Course Number</label>
                              <div class="col-md-4">
                              <input id="number" name="number" type="text" placeholder="" value="' . $plan['courseNumber'] . '" class="form-control input-md">

                              </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="director">Director</label>
                              <div class="col-md-4">
                              <input id="director" name="director" type="text" readonly placeholder="" value="' . $plan['Director'] . '" class="form-control input-md">

                              </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="directorID">Director User ID</label>
                              <div class="col-md-4">
                              <input id="directorID" name="directorID" type="text" placeholder="" value="' . $plan['created_user_id'] . '" class="form-control input-md">

                              </div>
                            </div>

                            <!-- Select Basic -->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="status">Status</label>
                              <div class="col-md-4">
                                <select id="status" name="status" class="form-control">';

                            $options = ['create', 'approve', 'runningScoring', 'complete', 'cancel'];
                            foreach($options as $option) {
                                $result .= '<option value="' . $option . '" ' . ($option ==$plan['status'] ? 'selected' : '') . '>' . $option . '</option>';
                            }

                            $result .= '
                                </select>
                              </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                              <label class="col-md-4 control-label" for="submitChanges"></label>
                              <div class="col-md-4">
                                <button id="submitChanges" name="submitChanges" class="btn btn-primary">Submit</button>
                              </div>
                            </div>

                            </fieldset>
                            </form>';

            return $result;
        } else {
            throw new exception("Could not generate edit form");
        }
    }

    public function generateEditModalAction() {
      $command = $this->pdo->prepare('SELECT sess.id, sess.start_datetime, sess.end_datetime, sess.status, sess.location, scen.name as ScenName
                                  FROM appquantumdb.judging_plan_session				sess
                                      INNER JOIN appquantumdb.judging_plan				plan
                                          ON	sess.judging_plan_id = plan.id
                                      INNER JOIN appquantumdb.organization				org
                                          ON	plan.organization_id = org.id
                                      INNER JOIN appquantumdb.scenario					scen
                                          ON	plan.scenario_id = scen.id
                                  WHERE sess.id = :id;');

      if ($command->execute(array(":id"=>$this->data['id']))) {
        $session = $command->fetch(PDO::FETCH_ASSOC);
        $result = '<form class="form-horizontal" onsubmit="return false;">
                        <fieldset>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="id">Session ID</label>
                          <div class="col-md-4">
                          <input id="id" name="id" type="text" readonly placeholder="" value="' . $session['id'] . '" class="form-control input-md">

                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="status">Status</label>
                          <div class="col-md-4">
                          <input id="status" name="status" type="text" placeholder="" value="' . $session['status'] . '" class="form-control input-md">

                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="start_datetime">Start Date-time</label>
                          <div class="col-md-4">
                          <input id="start_datetime" name="start_datetime" type="text" placeholder="" value="' . $session['start_datetime'] . '" class="form-control input-md">

                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="end_datetime">End Date-time</label>
                          <div class="col-md-4">
                          <input id="end_datetime" name="end_datetime" type="text" placeholder="" value="' . $session['end_datetime'] . '" class="form-control input-md">

                          </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="location">Location</label>
                          <div class="col-md-4">
                          <input id="location" name="location" type="text" placeholder="" value="' . $session['location'] . '" class="form-control input-md">

                          </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="editTheSession"></label>
                          <div class="col-md-4">
                            <button id="editTheSession" name="editTheSession" class="btn btn-primary">Submit</button>
                          </div>
                        </div>

                        </fieldset>
                        </form>';

        return $result;
      } else {
        throw new exception("Unable to pull session details");
      }
    }

    public function editJudgingPlanAction() {
        $command = $this->pdo->prepare('UPDATE appquantumdb.judging_plan
                                            SET courseTitle = :courseTitle, courseNumber = :courseNumber, created_user_id = :directorID, status = :status
                                            WHERE id = :id');

        if ($command->execute(array(":courseTitle"=>$this->data['courseTitle'], ":courseNumber"=>$this->data['courseNumber'],
                                        ":directorID"=>$this->data['directorID'], ":status"=>$this->data['status'],
                                        ":id"=>$this->data['id']))) {
            return "success";
        } else {
            throw new exception("Judging Plan could not be updated");
        }
    }

    public function editSessionAction() {
        $command = $this->pdo->prepare('UPDATE appquantumdb.judging_plan_session
                                            SET location = :location, start_datetime = :start_datetime, end_datetime = :end_datetime, status = :status
                                            WHERE id = :id');

        if ($command->execute(array(":location"=>$this->data['location'], ":start_datetime"=>$this->data['start_datetime'],
                                        ":end_datetime"=>$this->data['end_datetime'], ":status"=>$this->data['status'],
                                        ":id"=>$this->data['id']))) {
            return "success";
        } else {
            throw new exception("Session could not be updated");
        }
    }

}
