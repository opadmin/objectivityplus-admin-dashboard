<?php
use \ForceUTF8\Encoding;
use Mailgun\Mailgun;
use Twilio\Rest\Client;


class Conferencing
{
    private $_params;
    private $data;
    private $pdo;
    private $user;

    public function __construct($params,$user)
    {
        $this->_params = $params;
        $this->data = !isset($params['data']) ?: $params['data'];
        $this->user = $user;
        //Open database connection
        $this->pdo = op_pdoConn::getConnection();
    }

    public function submitSessionsAction() {
        $IDs = $this->data["IDs"];
//        error_log(print_R($IDs, true));
        foreach($IDs as $dimension)
        {

          for ($i = 0; $i < sizeof($dimension); ++$i) {
              $session = substr((strval($dimension[$i][0])), 8) ;
              $command = $this->pdo->prepare('CALL Quantum.clearSession(:session);');
              $command->execute(array(":session"=>$session));
  //            error_log("Session " . $session . " cleared");
              if ($command->execute(array(':session'=>$session))) {
                  for ($j = 3; $j < sizeof($dimension[$i]); ++$j) {
  //                    error_log($IDs[0][$i][$j]);
                      if (strpos(strval($dimension[$i][$j]), "I:") !== false && (strpos(strval($dimension[$i][$j]), "I:") === 0)) {
                          $command = $this->pdo->prepare('CALL Quantum.insertFaculty(:session, :instructor);');
                          $command->execute(array(':session'=>$session, ":instructor"=>substr(strval($dimension[$i][$j]), 2)));
  //                        error_log("Instructor " . substr(strval($IDs[0][$i][$j]), 2) . " added");
                      }

                      if (strpos(strval($dimension[$i][$j]), "S:") !== false && (strpos(strval($dimension[$i][$j]), "S:") === 0)) {
                          $command = $this->pdo->prepare('CALL Quantum.insertStudent(:session, :student);');
                          $command->execute(array(':session'=>$session, ":student"=>substr(strval($dimension[$i][$j]), 2)));
  //                        error_log("Student " . substr(strval($IDs[0][$i][$j]), 2) . " added");
                      }
                  }
              }  else {
                  throw new exception("Cannot access session");
              }
          }
        }

        return "success";
    }

    public function pullScenarioDaysAction() {

        $org = $this->data["organization"];
        $start = $this->data["startDate"];
        $end = $this->data["endDate"];

        $command = $this->pdo->prepare('SELECT DISTINCT(DATE_FORMAT(sess.start_datetime,\'%m/%d/%Y\')) as Dates, plan.id as id, plan.courseTitle
                                            FROM appquantumdb.judging_plan_session				sess
                                                INNER JOIN appquantumdb.judging_plan				plan
                                                    ON	sess.judging_plan_id = plan.id
                                                    AND plan.status != \'cancel\'
                                                INNER JOIN appquantumdb.organization				org
                                                    ON	plan.organization_id = org.id
                                                INNER JOIN appquantumdb.scenario					scen
                                                    ON	plan.scenario_id = scen.id
                                            WHERE org.name = :organization
                                            AND DATE_FORMAT(sess.start_datetime,\'%m/%d/%Y\') > :date1
                                            AND	DATE_FORMAT(sess.end_datetime,\'%m/%d/%Y\') <= :date2
                                            ORDER BY sess.start_datetime, sess.location;');

        $command->execute(array(":organization"=>$org, ":date1"=>$start, ":date2"=>$end));

        $scenarios = $command->fetchAll(PDO::FETCH_ASSOC);

        $result = '';

        foreach ($scenarios as $row) {
            $result .= '<option id="Selector' . str_replace('/', '', $row['Dates']) . '" data-plan="'. $row['id'] . '" value="' . $row['Dates'] . '">' . $row['Dates'] . ', ' . $row['courseTitle'] . '</option>';
        }

        return $result;
    }

    public function loadColumnsAction() {
        $date = $this->data["date"];

        $command = $this->pdo->prepare('SELECT sess.id as SessionID, sess.start_datetime, sess.location
                                          FROM appquantumdb.judging_plan_session               sess
                                              INNER JOIN appquantumdb.judging_plan                 jud
                                                ON jud.status != \'cancel\'
                                                AND jud.id = :id
                                                and sess.judging_plan_id = jud.id
                                          WHERE DATE_FORMAT(sess.start_datetime, \'%m/%d/%Y\') = :date; ORDER BY sess.location');

        if ($command->execute(array(":date"=>$date, ":id"=>$this->data["id"]))) {
            $sessions = $command->fetchAll(PDO::FETCH_ASSOC);

            $result = array();

            foreach ($sessions as $parent_row) {
                $resulting_sessions = '';
                $resulting_sessions .= '<div class="col-md-12 nopadding memberList text-center" id="' . $parent_row['SessionID'] . '"> <div data-datetime="'. $parent_row['start_datetime'] .'" data-location="'. $parent_row['location'] .'" class="sessionTitle col-md-12 text-center"><div class="row">' . date("g:i A",strtotime($parent_row['start_datetime'] . " -5hours") ) . '</div><div class="row">' . $parent_row['location'] . '</div></div>';

                $command = $this->pdo->prepare('SELECT usr.id as UserID, CONCAT(`last_name`, \', \', `first_name`) AS Instructor
                                          FROM appquantumdb.judging_plan                          jud
                                            INNER JOIN appquantumdb.judging_plan_session           sess
                                              ON jud.id = sess.judging_plan_id
                                              AND jud.id = :id
                                            INNER JOIN appquantumdb.judging_plan_session_faculty  fac
                                              ON fac.judging_plan_id = sess.id
                                            INNER JOIN appquantumdb.user_main                     usr
                                              ON fac.user_id = usr.id
                                            WHERE sess.id = :date
                                            AND jud.status != "cancel";');

                if ($command->execute(array(":date"=>$parent_row['SessionID'], ":id"=>$this->data["id"]))) {
                    $instructors = $command->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($instructors as $row) {
                        $resulting_sessions .= '<div class="col-md-11 person Draggable instructor" id="' . $row['UserID'] . '"><i id="' . $row['UserID'] . 'present" class="fa fa-check-square-o check icon hidden" aria-hidden="true"></i><i id="' . $row['UserID'] . 'absent" class="fa fa-times-circle-o x icon hidden" aria-hidden="true"></i>' . $row['Instructor'] . '<div class="Attendance"><div class="Present">Present</div><div class="Absent">Absent</div></div></div>';
                    }
                } else {
                    throw new exception("Unable to reach database");
                }

                $command = $this->pdo->prepare('SELECT usr.id as UserID, CONCAT(`last_name`, \', \', `first_name`) AS Student
                                          FROM appquantumdb.judging_plan_session               sess
                                              INNER JOIN appquantumdb.judging_plan                 jud
                                                ON jud.status != \'cancel\'
                                                and sess.judging_plan_id = jud.id
                                                AND jud.id = :id
                                              INNER JOIN appquantumdb.judging_plan_session_student stu
                                                ON stu.judging_plan_id = sess.id
                                              INNER JOIN appquantumdb.user_main                    usr
                                                ON usr.id = stu.user_id
                                          WHERE sess.id = :date
                                          AND jud.status != "cancel";');

                if ($command->execute(array(":date"=>$parent_row['SessionID'], ":id"=>$this->data["id"]))) {
                    $students = $command->fetchAll(PDO::FETCH_ASSOC);

                    foreach ($students as $row) {
                        $resulting_sessions .= '<div class="col-md-11 person Draggable student" id="' . $row['UserID'] . '"><i id="' . $row['UserID'] . 'present" class="fa fa-check-square-o check icon hidden" aria-hidden="true"></i><i id="' . $row['UserID'] . 'absent" class="fa fa-times-circle-o x icon hidden" aria-hidden="true"></i>' . $row['Student'] . '<div class="Attendance"><div class="Present">Present</div><div class="Absent">Absent</div></div></div>';
                    }
                } else {
                    throw new exception("Unable to reach database");
                }

                array_push($result, $resulting_sessions);

            }

            return $result;
        } else {
            throw new exception("Unable to reach sessions");
        }
    }

    public function pullUncommittedUsersAction() {
      $command = $this->pdo->prepare('SELECT JPS.user_id AS JudgingPlanSessionUserID, usr.id as UserID, CONCAT(`last_name`, \', \', `first_name`) AS Draggable
                                        FROM appquantumdb.user_main usr
                                        INNER JOIN Quantum.user_main_organization inter
                                          ON  usr.id = inter.organization_id
                                        INNER JOIN appquantumdb.organization org
                                          ON inter.user_id = org.id
                                        LEFT JOIN judging_plan_session_student JPS ON JPS.user_id = usr.id
                                        WHERE org.name = :organization
                                        ORDER BY last_name;');

      if ($command->execute(array(":organization"=>$this->data["organization"]))) {
        $instructors = $command->fetchAll(PDO::FETCH_ASSOC);

        $resulting_users = '';

        foreach ($instructors as $row) {



          $resulting_users .= '<div ' . ($row['JudgingPlanSessionUserID'] !== null ? "data-jps= " . $row['JudgingPlanSessionUserID'] : "") . ' class="col-md-3 smpadding person Draggable neutral ' . ($row['JudgingPlanSessionUserID'] !== null ? "hidden" : "") . '" id="' . $row['UserID'] . '">' . $row['Draggable'] . '</div>';
        }

        return $resulting_users;
      } else {
        throw new exception("Unable to reach database");
      }
    }

    public function getStartDateAction() {
      $org = $this->data["organization"];

      $command = $this->pdo->prepare('SELECT DATE_FORMAT(sess.start_datetime, \'%m\') as Mth, (DATE_FORMAT(sess.start_datetime, \'%Y\')) as Year
                                          FROM appquantumdb.judging_plan_session				sess
                                              INNER JOIN appquantumdb.judging_plan				plan
                                                  ON	sess.judging_plan_id = plan.id
                                              INNER JOIN appquantumdb.organization				org
                                                  ON	plan.organization_id = org.id
                                              INNER JOIN appquantumdb.scenario					scen
                                                  ON	plan.scenario_id = scen.id
                                          WHERE org.name = :organization
                                          ORDER BY start_datetime DESC
                                          LIMIT 1;');

      $command->execute(array(":organization"=>$org));

      $date = $command->fetchAll(PDO::FETCH_ASSOC);

      return ($date[0]["Year"] . '-' . $date[0]["Mth"] . '-01' );
    }

    public function getSessionsAction() {
      $date = $this->data["date"];
//      error_log($this->data["date"]);
//      error_log($this->data["id"]);

      $command = $this->pdo->prepare('SELECT sess.id as SessionID, sess.start_datetime, sess.location
                                        FROM appquantumdb.judging_plan_session               sess
                                            INNER JOIN appquantumdb.judging_plan                 jud
                                              ON jud.status != \'cancel\'
                                              AND jud.id = :id
                                              and sess.judging_plan_id = jud.id
                                        WHERE DATE_FORMAT(sess.start_datetime, \'%m/%d/%Y\') = :date;');

      if ($command->execute(array(":date"=>$date, ":id"=>$this->data["id"]))) {
          $sessions = $command->fetchAll(PDO::FETCH_ASSOC);
          $i = 2;
          $resulting_sessions = '';

          foreach ($sessions as $parent_row) {

              if ( $i % 2 == 0) {
                $resulting_sessions .= '<div class="col-md-12 smPadTop mdPadBot text-center" data-id="' . $parent_row['SessionID'] . '">' . $parent_row['location'] . ', ' . $parent_row['start_datetime'] . '<div class"pull-right" role="group"><button type="button" class="btn btn-opblue btn-sm smsButton">Send SMS</button> <button type="button" class="btn btn-opblue btn-sm mailButton">Send Email</button></div></div>';
              } else  {
                $resulting_sessions .= '<div class="col-md-12 smPadTop mdPadBot text-center" data-id="' . $parent_row['SessionID'] . '">' . $parent_row['location'] . ', ' . $parent_row['start_datetime'] . '<div class"pull-right" role="group"><button type="button" class="btn btn-opblue btn-sm smsButton">Send SMS</button> <button type="button" class="btn btn-opblue btn-sm emailButton">Send Email</button></div></div>';
              }
            ++$i;
          }

          return $resulting_sessions;
      } else {
          throw new exception("Unable to reach sessions");
      }
    }

    public function getMessageTextAction() {
      if ($this->data["sms"]) {
        $type = 'SMS';
      } else {
        $type = 'Email';
      }

      $command = $this->pdo->prepare('SELECT Question FROM Quantum.Website WHERE Category = :type;');

      if ($command->execute(array(":type"=>$type))) {
          $text = $command->fetchAll(PDO::FETCH_ASSOC);
          return $text[0]['Question'];
      } else {
          throw new exception("Cannot reach database");
      }

    }

    public function sendSMSAction()   {
        $command = $this->pdo->prepare("SELECT usr.*
                                          FROM appquantumdb.judging_plan                          jud
                                            INNER JOIN appquantumdb.judging_plan_session           sess
                                              ON sess.id = :id
                                              AND jud.id = sess.judging_plan_id
                                            INNER JOIN appquantumdb.judging_plan_session_faculty  fac
                                              ON fac.judging_plan_id = sess.id
                                            INNER JOIN appquantumdb.user_main                     usr
                                              ON fac.user_id = usr.id
                                            WHERE jud.status != \"cancel\"
                                            AND usr.phone_number IS NOT NULL
                                            AND usr.phone_number != \"\"
                                        UNION
                                        SELECT usr.*
                                          FROM appquantumdb.judging_plan                          jud
                                            INNER JOIN appquantumdb.judging_plan_session           sess
                                              ON sess.id = :id
                                              AND jud.id = sess.judging_plan_id
                                            INNER JOIN appquantumdb.judging_plan_session_student fac
                                              ON fac.judging_plan_id = sess.id
                                            INNER JOIN appquantumdb.user_main                     usr
                                              ON fac.user_id = usr.id
                                            WHERE jud.status != \"cancel\"
                                            AND usr.phone_number IS NOT NULL
                                            AND usr.phone_number != \"\";");

        if ($command->execute(array("id"=>$this->data['session']))) {
            $account_sid = 'AC61fda4b9f026d5d2f2105a953081b579';
            $auth_token = 'd8dceab33ededfe4e261d12bab760d7b';
            $client = new Client($account_sid, $auth_token);

            while ($row = $command->fetch()) {
                $text = $this->data['message'];
                $text = str_replace('%name%', $row["first_name"], $text);
                $text = str_replace('%time%', $this->data["time"], $text);

                $client->messages->create(
                    $row['phone_number'],
                    array(
                        'from' => "+13374437070",
                        "body" => $text
                    ));
                error_log("Message sent to:" . $row['phone_number']);
            }
        }
    }

    public function sendEmailAction() {
        $command = $this->pdo->prepare("SELECT usr.username, usr.first_name
                                          FROM appquantumdb.judging_plan                          jud
                                            INNER JOIN appquantumdb.judging_plan_session           sess
                                              ON sess.id = :id
                                              AND jud.id = sess.judging_plan_id
                                            INNER JOIN appquantumdb.judging_plan_session_faculty  fac
                                              ON fac.judging_plan_id = sess.id
                                            INNER JOIN appquantumdb.user_main                     usr
                                              ON fac.user_id = usr.id
                                            WHERE jud.status != \"cancel\"
                                        UNION
                                        SELECT usr.username, usr.first_name
                                          FROM appquantumdb.judging_plan                          jud
                                            INNER JOIN appquantumdb.judging_plan_session           sess
                                              ON sess.id = :id
                                              AND jud.id = sess.judging_plan_id
                                            INNER JOIN appquantumdb.judging_plan_session_student fac
                                              ON fac.judging_plan_id = sess.id
                                            INNER JOIN appquantumdb.user_main                     usr
                                              ON fac.user_id = usr.id
                                            WHERE jud.status != \"cancel\";");

        if ($command->execute(array(":id"=>$this->data["session"]))) {
            $users = $command->fetchAll(PDO::FETCH_ASSOC);

            $mgClient = new Mailgun('key-ca326a478754b7447e178fdb37fc2be0');
            $domain = "objectivityplus.com";

            foreach ($users as $user) {
                $text = $this->data['message'];
                $text = str_replace('%name%', $user["first_name"], $text);
                $text = str_replace('%time%', strval($this->data["time"]), $text);
                $mgClient->sendMessage($domain, array(
                    'from'    => 'Dero Kratzberg <dero@objectivityplus.com>',
                    'to'      => $user['username'],
                    'subject' => 'Scheduled Evaluation',
                    'text'    => $text
                ));

            }

        } else {
            throw new exception("Could not reach database. Contact support@objectivityplus.com");
        }
    }

    public function findUserAction() {
        $command = $this->pdo->prepare('SELECT sess.start_datetime, jud.courseTitle
                                          FROM appquantumdb.judging_plan_session               sess
                                              INNER JOIN appquantumdb.judging_plan                 jud
                                                ON jud.status != \'cancel\'
                                                and sess.judging_plan_id = jud.id
                                              INNER JOIN appquantumdb.judging_plan_session_student stu
                                                ON stu.judging_plan_id = sess.id
                                              INNER JOIN appquantumdb.user_main                    usr
                                                ON usr.first_name = :first_name
                                                AND usr.last_name = :last_name
                                                AND usr.id = stu.user_id
                                          WHERE (DATE_FORMAT(sess.start_datetime, \'%m/%d/%Y\') > :start_date AND DATE_FORMAT(sess.start_datetime, \'%m/%d/%Y\') < :end_date)
                                          AND jud.status != "cancel";');

        $results = '';

        if ($command->execute(array(":start_date"=>$this->data['startDate'], ":end_date"=>$this->data['endDate'], ":first_name"=>$this->data['first_name'], ":last_name"=>$this->data['last_name']))) {
            $students = $command->fetchAll(PDO::FETCH_ASSOC);

            foreach ($students as $row) {
                $results .= date("g:i A",strtotime($row['start_datetime'] . " -5hours") ) . ', ' . $row['courseTitle'];
            }
        } else {
            throw new exception("Unable to reach database");
        }

        return $results;
    }

    public function pullCompletedStudentsAction() {
        $connection = $this->pdo->prepare("SELECT CONCAT(`last_name`, ', ', `first_name`) as name, sess.id, usr.id as user_id, sess.start_datetime
                                            FROM 		appquantumdb.judging_plan							plan
                                            INNER JOIN 	appquantumdb.judging_plan_session					sess
                                                ON	plan.id = sess.judging_plan_id
                                                AND plan.organization_id = :id
                                            INNER JOIN	appquantumdb.judging_plan_session_student			res
                                                ON	sess.id = res.judging_plan_id
                                            INNER JOIN	appquantumdb.user_main								usr
                                                ON	res.user_id = usr.id
                                            WHERE DATE_FORMAT(sess.start_datetime,'%m/%d/%Y') > :date1
                                            AND	DATE_FORMAT(sess.end_datetime,'%m/%d/%Y') <= :date2
                                            ORDER BY sess.start_datetime DESC, last_name ASC;");

        if ($connection->execute(array(":id"=>$this->data['organization'], ":date1"=>$this->data["startDate"], ":date2"=>$this->data["endDate"]))) {
            return $connection->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return '';
        }
    }

    public function loadResultsAction() {


        $connection = $this->pdo->prepare("SELECT DISTINCT(res.faculty_id)
                                            FROM 		appquantumdb.judging_plan_session					sess
                                            INNER JOIN	appquantumdb.judging_plan_session_result			res
                                                ON	sess.id = :id
                                                AND sess.`status` != \"Cancelled\"
                                                AND	sess.id = res.session_id
                                            INNER JOIN	appquantumdb.judging_plan_session_result_criterion	cri
                                                ON 	res.id = cri.result_id
                                            INNER JOIN 	appquantumdb.scenario_criteria						scen
                                                ON	cri.criterion_id = scen.id
                                            ORDER BY scen.order_id;");

        if($connection->execute(array(":id"=>$this->data["session"]))) {
            $faculty = $connection->fetchAll(PDO::FETCH_ASSOC);
            
            $results = [];

            foreach($faculty as $teacher) {
                $connection = $this->pdo->prepare("SELECT res.id, scen.text, cri.checked, CONCAT(`last_name`, ', ', `first_name`) AS name
                                                        FROM 		appquantumdb.judging_plan_session_result			res
                                                        INNER JOIN	appquantumdb.judging_plan_session_result_criterion	cri
                                                            ON	res.session_id = :session
                                                            AND res.student_id = :student
                                                            AND res.faculty_id = :faculty
                                                            AND	res.id = cri.result_id
                                                        INNER JOIN 	appquantumdb.scenario_criteria						scen
                                                            ON	cri.criterion_id = scen.id
                                                        INNER JOIN appquantumdb.user_main								usr
                                                            ON usr.id = res.faculty_id
                                                        ORDER BY scen.order_id;");
                error_log($teacher["faculty_id"]);
                error_log($this->data["user_id"]);
                error_log($this->data["session"]);
                if($connection->execute(array(":faculty"=>$teacher["faculty_id"], ":student"=>$this->data["user_id"], ":session"=>$this->data["session"]))) {
                    array_push($results, $connection->fetchAll(PDO::FETCH_ASSOC));
                }
            }
            
            return $results;
        }
        return '';
    }
}
