<?php
use \ForceUTF8\Encoding;
use Mailgun\Mailgun;

class Reset
{
    private $_params;
    private $data;
    private $pdo;
    private $user;

    public function __construct($params,$user)
    {
        $this->_params = $params;
        $this->data = !isset($params['data']) ?: $params['data'];
        $this->user = $user;
        //Open database connection
        //$this->pdo = op_pdoConn::getConnection();
    }

    public function verifyEmailAction() {
        $_host = "db2.apexinnovations.com";
        $connection = new PDO("mysql:host={$_host};dbname=Quantum;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $cmd = $connection->prepare('SELECT "yes" FROM Quantum.Employees WHERE email = :email;');

        if ($cmd->execute(array(":email"=>$this->data['email']))) {
            $result = $cmd->fetch(PDO::FETCH_ASSOC);

            if ($result) {
                $mgClient = new Mailgun('key-ca326a478754b7447e178fdb37fc2be0');
                $domain = "objectivityplus.com";

                $temp = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                    // 32 bits for "time_low"
                    mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

                    // 16 bits for "time_mid"
                    mt_rand( 0, 0xffff ),

                    // 16 bits for "time_hi_and_version",
                    // four most significant bits holds version number 4
                    mt_rand( 0, 0x0fff ) | 0x4000,

                    // 16 bits, 8 bits for "clk_seq_hi_res",
                    // 8 bits for "clk_seq_low",
                    // two most significant bits holds zero and one for variant DCE1.1
                    mt_rand( 0, 0x3fff ) | 0x8000,

                    // 48 bits for "node"
                    mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
                );

                $mgClient->sendMessage($domain, array(
                    'from'    => 'Tech Support <developers@objectivityplus.com>',
                    'to'      => $this->data['email'],
                    'subject' => 'Password Reset Token',
                    'text'    => ('Copy and past the following token into the token input box on the password reset page: ' . strval($temp))
                ));

                $cmd = $connection->prepare('UPDATE Quantum.Employees SET token = :token WHERE email = :email;');

                if ($cmd->execute(array(":email"=>$this->data['email'], ":token"=>$temp))) {
                } else {
                    throw new exception("Error processing token");
                }

                return "<form class=\"form-horizontal\">
                            <fieldset>

                            <!-- Form Name -->
                            <legend>Token Validation</legend>

                            <!-- Text input-->
                            <div class=\"form-group\">
                              <label class=\"col-md-4 control-label\" for=\"token\">Paste Token Here:</label>
                              <div class=\"col-md-4\">
                              <input name=\"token\" class=\"form-control input-md\" id=\"token\" type=\"text\" placeholder=\"\">

                              </div>
                            </div>

                            <!-- Text input-->
                            <div class=\"form-group\">
                              <label class=\"col-md-4 control-label\" for=\"token\">New Password:</label>
                              <div class=\"col-md-4\">
                              <input name=\"password1\" class=\"form-control input-md\" id=\"password1\" type=\"password\" placeholder=\"\">

                              </div>
                            </div>

                            <!-- Text input-->
                            <div class=\"form-group\">
                              <label class=\"col-md-4 control-label\" for=\"token\">New Password Again:</label>
                              <div class=\"col-md-4\">
                              <input name=\"password2\" class=\"form-control input-md\" id=\"password2\" type=\"password\" placeholder=\"\">

                              </div>
                            </div>

                            <!-- Button -->
                            <div class=\"form-group\">
                              <label class=\"col-md-4 control-label\" for=\"submitToken\"></label>
                              <div class=\"col-md-4\">
                                <button name=\"submitToken\" class=\"btn btn-primary\" id=\"submitToken\">Submit</button>
                              </div>
                            </div>

                            </fieldset>
                        </form>";
            } else {
                throw new exception("Email address not found");
            }

        } else {
            throw new exception("Couldn't fetch results");
        }
    }

    public function verifyTokenAction() {
        $_host = "db2.apexinnovations.com";
        $connection = new PDO("mysql:host={$_host};dbname=Quantum;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $cmd = $connection->prepare('SELECT token FROM Quantum.Employees WHERE email = :email;');

        if($cmd->execute(array(":email"=>$this->data['email']))) {
            $token = $cmd->fetch(PDO::FETCH_ASSOC);
        } else {
            throw new exception("Cannot reach server");
        }

        if ($this->data['token'] == $token['token']) {

            $cmd = $connection->prepare('UPDATE Quantum.Employees SET password = :password, token = "" WHERE email = :email;');

            $password = password_hash($this->data['password'], PASSWORD_BCRYPT);

            if($cmd->execute(array(":password"=>$password, ":email"=>$this->data['email']))) {
                return "success";
            } else {
                throw new exception("Unable to update password");
            }
        } else {
            throw new exception("Token mismatch. Check your token or try reset again");
        }
    }

}
