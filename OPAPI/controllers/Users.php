<?php
use \ForceUTF8\Encoding;

class Users
{
    private $_params;
    private $data;
    private $pdo;
    private $user;

    public function __construct($params,$user)
    {
        $this->_params = $params;
        $this->data = !isset($params['data']) ?: $params['data'];
        $this->user = $user;
        //Open database connection
        $this->pdo = op_pdoConn::getConnection();
    }

    public function deleteAction()
    {
        $delete = $this->pdo->prepare('DELETE FROM appquantumdb.user_main WHERE id = ?');
        $delete->bindParam(1,$this->data['user_id']);
        if (!$delete->execute())
        {
            throw new Exception('Could not delete the user');
        }
        $results=$delete->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    public function pullUsersAction()   {

        if (isset($this->_params['userEmail'])) {

            $user = $this->_params['userEmail'];

            $command = $this->pdo->prepare('SELECT usr.id, username, first_name, middle_name, last_name, address1, address2, city, state, postal_code, usr.phone_number
                                            FROM appquantumdb.user_main 							usr
                                            LEFT JOIN appquantumdb.contact_street_address			ctc
                                                ON usr.contact_info_id = ctc.ID
                                            WHERE username = :username;');

            $command->execute(array("username" => $user));

            $userInfo = $command->fetchAll(PDO::FETCH_ASSOC);

            $results = ['form' => '<form class="form-horizontal" method="POST" action="../scripts/php/updateUser.php">
                                <fieldset>

                                    <!-- Form Name -->
                                    <legend>Edit User</legend>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="user_id">User ID</label>
                                        <div class="col-md-4">
                                            <input id="user_id" name="user_id" type="text" placeholder="" class="form-control input-md" value="' . $userInfo[0]['id'] . '" readonly>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="username">Username/Email</label>
                                        <div class="col-md-4">
                                            <input id="username" name="username" type="text" placeholder="" class="form-control input-md" value="' . $userInfo[0]['username'] . '" readonly>
                                        </div>
                                    </div>

                                    <!-- Password input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="password">Password</label>
                                        <div class="col-md-4">
                                            <input id="password" name="password" type="password" placeholder="Current unable to reset due to encryption problems" class="form-control input-md" readonly>
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="first_name">First name</label>
                                        <div class="col-md-4">
                                            <input id="first_name" name="first_name" type="text" placeholder="" value="' . $userInfo[0]['first_name'] . '" class="form-control input-md">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="middle_name">Middle Name</label>
                                        <div class="col-md-4">
                                            <input id="middle_name" name="middle_name" type="text" placeholder="" value="' . $userInfo[0]['middle_name'] . '" class="form-control input-md">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="last_name">Last Name</label>
                                        <div class="col-md-4">
                                            <input id="last_name" name="last_name" type="text" placeholder="" value="' . $userInfo[0]['last_name'] . '" class="form-control input-md">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="address_1">Address</label>
                                        <div class="col-md-4">
                                            <input id="address_1" name="address_1" type="text" placeholder="" class="form-control input-md" value="' . $userInfo[0]['address1'] . '">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="address_2">Address Line 2</label>
                                        <div class="col-md-4">
                                            <input id="address_2" name="address_2" type="text" placeholder="" class="form-control input-md"  value="' . $userInfo[0]['address2'] . '">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="city">City</label>
                                        <div class="col-md-4">
                                            <input id="city" name="city" type="text" placeholder="" class="form-control input-md" value="' . $userInfo[0]['city'] . '">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="State">State</label>
                                        <div class="col-md-4">
                                            <input id="State" name="State" type="text" placeholder="" class="form-control input-md" value="' . $userInfo[0]['state'] . '">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="zip">Zip</label>
                                        <div class="col-md-4">
                                            <input id="zip" name="zip" type="text" placeholder="" class="form-control input-md" value="' . $userInfo[0]['postal_code'] . '">
                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="phone">Phone</label>
                                        <div class="col-md-4">
                                            <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md" value="' . $userInfo[0]['phone_number'] . '">
                                        </div>
                                    </div>

                                    <!-- Button -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="submit"></label>
                                        <div class="col-md-4">
                                            <button id="submit" name="submit" class="btn btn-primary">Submit</button>
                                        </div>

                                        <label class="col-md-4 control-label" for="delbutton"></label>
                                        <div class="col-md-4">
                                            <button id="delbutton" name="delbutton" class="btn btn-danger">Delete User</button>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>'];
            return $results;
        }
        else {
            throw new Exception('User info not submitted.');
        }
    }

    public function pullUsersForOrgAction() {
        try {
            if (isset($this->_params['organization'])) {

                $command = $this->pdo->prepare('SELECT DISTINCT(username) AS username
                                                        FROM appquantumdb.user_main 				usr
                                                         INNER JOIN appquantumdb.judging_plan 		jud
                                                            ON usr.id = jud.created_user_id
                                                         INNER JOIN appquantumdb.organization 		org
                                                            ON org.id = jud.organization_id
                                                        WHERE org.name = :organization
                                                UNION
                                                SELECT DISTINCT(username) AS username
                                                        FROM appquantumdb.user_main 					usr
                                                         LEFT JOIN appquantumdb.judging_plan_student	stud
                                                            ON usr.id = stud.user_id
                                                        LEFT JOIN appquantumdb.judging_plan_faculty		fac
                                                            ON usr.id = fac.user_id
                                                         LEFT JOIN appquantumdb.judging_plan	 		jud
                                                            ON jud.id = stud.judging_plan_id
                                                         LEFT JOIN appquantumdb.organization 			org
                                                            ON org.id = jud.organization_id
                                                        WHERE org.name = :organization
                                                UNION
                                                SELECT DISTINCT(username) AS username
                                                        FROM appquantumdb.user_main 					usr
                                                        LEFT JOIN appquantumdb.judging_plan_faculty		fac
                                                            ON usr.id = fac.user_id
                                                         LEFT JOIN appquantumdb.judging_plan	 		jud
                                                            ON jud.id = fac.judging_plan_id
                                                         LEFT JOIN appquantumdb.organization 			org
                                                            ON org.id = jud.organization_id
                                                        WHERE org.name = :organization
                                                        ORDER BY username;');
                $command->bindParam(':organization', $this->_params['organization'], PDO::PARAM_STR);

                if ($command->execute()) {
                    $users = $command->fetchAll(PDO::FETCH_ASSOC);

                    $command = $this->pdo->prepare('SELECT DISTINCT(username) AS username
                                                FROM appquantumdb.user_main 				usr
                                                 LEFT JOIN appquantumdb.judging_plan 		jud
                                                    ON usr.id = jud.created_user_id
                                                WHERE jud.created_user_id IS NULL;');
                    $command->execute();
                    $unassignedUsers = $command->fetchAll(PDO::FETCH_ASSOC);

                    return ['users'=>$users,'unassignedUsers'=>$unassignedUsers];
                }

            }

        }
        catch (Exception $e){
            $message = 'Database error. Please try again later';
        }

    }

    public function pullOrganizationsAction() {
        $command = $this->pdo->prepare("SELECT `name`, id FROM appquantumdb.organization ORDER BY `name`;");

        if ($command->execute()) {
            $organizations = $command->fetchAll(PDO::FETCH_ASSOC);

            return $organizations;
        }

        return '';

    }

    public function newUserAction() {
      $command = $this->pdo->prepare('SELECT MAX(id + 1) as id FROM appquantumdb.user_main;');
      $command->execute();
      $id = $command->fetch();
      $id = $id["id"];

      $command = $this->pdo->prepare('SELECT id  FROM appquantumdb.organization WHERE name = :organization;');
      $command->execute(array(":organization"=>$this->data["organization"]));
      $org_id = $command->fetch();
      $org_id = $org_id["id"];

      $insertCommand = $this->pdo->prepare('INSERT INTO appquantumdb.user_main (id, username, first_name, middle_name, last_name, phone_number) VALUES ( :id, :email, :first_name, :middle_name, :last_name, :phone);');

      error_log(print_r($this->data,1));

      if(!isset($this->data["phone"]))
      {
        $this->data["phone"] = null;
      }

      $executableArray = array(":id"=>$id, ":email"=>$this->data["email"], ":first_name"=>$this->data["first_name"], ":middle_name"=>$this->data["middle_name"], ":last_name"=>$this->data["last_name"], ":phone"=>$this->data["phone"]);

        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
      if ($insertCommand->execute($executableArray)) {
          $command = $this->pdo->prepare('INSERT INTO appquantumdb.user_auth (id, password_hash, version) VALUES (:id, "$2a$08$gxS3OVsrz.Mf8Q2JEXdmheS2JfTR/avrDNDLsr9Qkk4evjjWdFwT2", "1");');
          if ($command->execute(array(":id"=>$id))) {
          } else {
            throw new exception("Could not set authentication");
          }

//          $command = $this->pdo->prepare('INSERT INTO appquantumdb.user_role (user_id, role_id) VALUES (:id, :role);');
//          if ($command->execute(array(":id"=>$id, ":role"=>$this->data["role"]))) {
//          } else {
//            throw new exception("Could not set roles");
//          }

          $command = $this->pdo->prepare('INSERT INTO appquantumdb.user_role (user_id, role_id) VALUES (:id, :role);');
          if ($command->execute(array(":id"=>$id, ":role"=>'52'))) {
          } else {
              throw new exception("Could not set roles");
          }

          $command = $this->pdo->prepare('INSERT INTO appquantumdb.contact_base (id, user_contact_info_id, name, version, type) VALUES (:id, :id2, "Primary", "1", "email");');
          if ($command->execute(array(":id"=>$id, ":id2"=>$id))) {
          } else {
            throw new exception("Could not reach contacts");
          }

          $command = $this->pdo->prepare('INSERT INTO appquantumdb.contact_email (id, email) VALUES (:id, :email);');
          if ($command->execute(array(":id"=>$id, ":email"=>$this->data["email"]))) {
          } else {
            throw new exception("Could not set email");
          }

          $command = $this->pdo->prepare('INSERT INTO Quantum.user_main_organization (user_id, organization_id) VALUES (:organization_id, :id);');
          if ($command->execute(array(":id"=>$id, ":organization_id"=>$org_id))) {
          } else {
            throw new exception("Could not set organization");
          }
          return $id;
      } else {
        // error_log(print_r($this->pdo->errorInfo(),1));
        throw new exception("Error inserting user into database. Error: " . $this->pdo->errorInfo()[2]);
      }
    }
}
