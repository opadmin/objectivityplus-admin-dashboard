<?php

class Revisions
{
    private $_params;
    private $data;
    private $pdo;
    private $user;

    public function __construct($params,$user)
    {
        $this->_params = $params;
        $this->data = !isset($params['data']) ?: $params['data'];
        $this->user = $user;
        //Open database connection
        $this->pdo =op_pdoConn::getConnection(true);
    }

    public function createAction()
    {
        //create a new content element
    }

    public function utf8_to_unicode($str) {

        $unicode = array();
        $values = array();
        $lookingFor = 1;

        for ($i = 0; $i < strlen($str); $i++) {

            $thisValue = ord($str[$i]);

            if ($thisValue < 128)
                $unicode[] = str_pad(dechex($thisValue), 4, "0", STR_PAD_LEFT);
            else {
                if (count($values) == 0) $lookingFor = ($thisValue < 224) ? 2 : 3;
                $values[] = $thisValue;
                if (count($values) == $lookingFor) {
                    $number = ($lookingFor == 3) ?
                        (($values[0] % 16) * 4096) + (($values[1] % 64) * 64) + ($values[2] % 64):
                        (($values[0] % 32) * 64) + ($values[1] % 64);
                    $number = strtoupper(dechex($number));
                    $unicode[] = str_pad($number, 4, "0", STR_PAD_LEFT);
                    $values = array();
                    $lookingFor = 1;
                } // if
            } // if
        } // for
        $str="";
        foreach ($unicode as $key => $value) {
            $str .= $value;
        }


        return ($str);
    } // utf8_to_unicode

    public function readContentAction()
    {
        //read all the content elements for an ID
        $read = $this->pdo->prepare("SELECT CONCAT(CONCAT(LEFT(U.FirstName,1),'.'), ' ', U.LastName) AS Name,E.PictureLocation, CH.Content, CH.PublishedDate, CH.ID as RevisionID FROM ace_ContentElements CH JOIN Users U ON CH.PublishedBy = U.ID LEFT JOIN Employees E ON E.Email = U.Login WHERE CH.ID = :contentID UNION ALL SELECT CONCAT(CONCAT(LEFT(U.FirstName,1),'.'), ' ', U.LastName) AS Name,E.PictureLocation, CH.Content, CH.PublishedDate, CH.ID AS RevisionID FROM ace_ContentHistory CH JOIN Users U ON CH.PublishedBy = U.ID LEFT JOIN Employees E ON E.Email = U.Login WHERE ContentID = :contentID ORDER BY PublishedDate DESC"); 
        // error_log(print_r($this->data,1));
        $read->execute([':contentID'=>$this->data]);
        $results=$read->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function testAction()
    {
        error_log('test success!');
    }

    public function updateAction()
    {
        //update a content element
        $this->data['moreContent'] = !isset($this->data['moreContent']) ?: null;
        $data = $this->data;
        $writeToStaging = $this->pdo->prepare("CALL sp_ContentStage(:stagingID,:pageID,:contentType,:contentElement,:moreContent,:userID)");

        $writeToStaging->execute(
            [
                ':stagingID'=>$data['stagingID'],
                ':pageID'=>$data['pageID'],
                ':contentType'=>$data['contentType'],
                ':contentElement'=>htmlspecialchars_decode(htmlentities($data['content'], ENT_NOQUOTES, 'UTF-8'), ENT_NOQUOTES),
                ':moreContent'=>$data['moreContent'],
                ':userID'=>$this->user->ID
            ]);

        $publish = $this->pdo->prepare("CALL sp_ContentPublish(:stagingID,:userID)");
        $publish->execute(
            [
                ':stagingID'=>$data['stagingID'],
                ':userID'=>$this->user->ID
            ]);


        
    }

    public function deleteAction()
    {
        //delete a content element
    }
}