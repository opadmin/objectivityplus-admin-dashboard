<?php

class Contenttypes
{
    private $_params;
    private $data;
    private $pdo;
    private $user;

    public function __construct($params,$user)
    {
        $this->_params = $params;
        // $this->data = !isset($params['data']) ?: $params['data'];
        $this->user = $user;
        //Open database connection
        $this->pdo =op_pdoConn::getConnection(true);
    }

    

    public function readContentTypesAction()
    {
        //read all the content elements for an ID
        $CT = $this->pdo->query("SELECT DISTINCT ace_ContentStaging.ContentType FROM ace_ContentStaging WHERE ContentType NOT IN ('instructions','audio','') ORDER BY ContentType"); 
        // error_log(print_r($this->data,1));
        // $read->execute();
        $contentTypes = ['ContentTypes'=>[]];
        foreach($CT as $contentType)
        {
            $contentTypes['ContentTypes'][] = $contentType['ContentType'];
        }
        $results = $contentTypes;

        return $results;
    }



}