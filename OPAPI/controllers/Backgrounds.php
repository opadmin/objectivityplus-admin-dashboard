<?php

class Backgrounds
{
    private $_params;
    private $data;
    private $pdo;
    private $user;

    public function __construct($params,$user)
    {
        $this->_params = $params;
        $this->data = !isset($params['data']) ?: $params['data'];
        $this->user = $user;
        //Open database connection
        $this->pdo =op_pdoConn::getConnection(true);
    }

    public function createAction()
    {
        //create a new content element
    }

    public function utf8_to_unicode($str) {

        $unicode = array();
        $values = array();
        $lookingFor = 1;

        for ($i = 0; $i < strlen($str); $i++) {

            $thisValue = ord($str[$i]);

            if ($thisValue < 128)
                $unicode[] = str_pad(dechex($thisValue), 4, "0", STR_PAD_LEFT);
            else {
                if (count($values) == 0) $lookingFor = ($thisValue < 224) ? 2 : 3;
                $values[] = $thisValue;
                if (count($values) == $lookingFor) {
                    $number = ($lookingFor == 3) ?
                        (($values[0] % 16) * 4096) + (($values[1] % 64) * 64) + ($values[2] % 64):
                        (($values[0] % 32) * 64) + ($values[1] % 64);
                    $number = strtoupper(dechex($number));
                    $unicode[] = str_pad($number, 4, "0", STR_PAD_LEFT);
                    $values = array();
                    $lookingFor = 1;
                } // if
            } // if
        } // for
        $str="";
        foreach ($unicode as $key => $value) {
            $str .= $value;
        }


        return ($str);
    } // utf8_to_unicode

    public function readBackgroundsAction()
    {
        $root = $_SERVER['DOCUMENT_ROOT'] . $this->data;
        // error_log(print_r($this->data,1));
        $files  = array('files'=>array(), 'dirs'=>array()); 
          $directories  = array(); 
          $last_letter  = $root[strlen($root)-1]; 
          $root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR; 
          
          $directories[]  = $root; 
          
          while (sizeof($directories)) { 
            $dir  = array_pop($directories); 
            if ($handle = opendir($dir)) { 
              while (false !== ($file = readdir($handle))) { 
                if ($file == '.' || $file == '..') { 
                  continue; 
                } 
                $file  = $dir.$file; 
                if (is_dir($file)) { 
                  $directory_path = $file.DIRECTORY_SEPARATOR; 
                  array_push($directories, $directory_path); 
                  $files['dirs'][]  = $directory_path; 
                } elseif (is_file($file)) { 
                  // $files['files'][]  = $file; 
                  $files['files'][]  = substr($file,strpos($file,$this->data)); 

                } 
              } 
              closedir($handle); 
            } 
          } 
          // error_log(print_r($files,1));
          return $files; 
    }

    public function testAction()
    {
        error_log('test success!');
    }

    public function updateAction()
    {
        //update a content element
        $this->data['moreContent'] = !isset($this->data['moreContent']) ?: null;
        $data = $this->data;
        $writeToStaging = $this->pdo->prepare("CALL sp_ContentStage(:stagingID,:pageID,:contentType,:contentElement,:moreContent,:userID)");

        $writeToStaging->execute(
            [
                ':stagingID'=>$data['stagingID'],
                ':pageID'=>$data['pageID'],
                ':contentType'=>$data['contentType'],
                ':contentElement'=>htmlspecialchars_decode(htmlentities($data['content'], ENT_NOQUOTES, 'UTF-8'), ENT_NOQUOTES),
                ':moreContent'=>$data['moreContent'],
                ':userID'=>$this->user->ID
            ]);

        $publish = $this->pdo->prepare("CALL sp_ContentPublish(:stagingID,:userID)");
        $publish->execute(
            [
                ':stagingID'=>$data['stagingID'],
                ':userID'=>$this->user->ID
            ]);


        
    }

    public function deleteAction()
    {
        //delete a content element
    }
}