<?php
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="scripts/socket.io.js" class=""></script>
    <script src="scripts/Cheshire.js" class=""></script>
    <script src="scripts/bootstrap/js/bootstrap.min.js" class=""></script>

    <script>
        $(document).ready(function(){
            $('#submit').on('click', function(e) {
                e.preventDefault();

                $.ajax({
                    method: "POST",
                    url: '/dashboard/OPAPI/index.php',
                    dataType:'json',
                    data: {
                        controller:'Reset',
                        action:'verifyEmail',
                        data : {
                            email : $('#inputEmail').val()
                        }
                    },
                    success: function (data) {
                        if(data.success)
                        {
                            alert("DO NOT LEAVE THIS PAGE. A token has been emailed to you. Copy and past it into the form that has appeared below then submit.")
                            $("#form2").html('');
                            $("#form2").html(data.data);
                            bindTokenSubmit();
                        }
                        else
                        {
                            alert(data.errormsg);
                        }
                    }
                });
            })

            bindTokenSubmit();
        })

        function bindTokenSubmit() {

            $('#submitToken').on("click", function(e) {
                e.preventDefault();

                if ($('#password1').val() == $('#password2').val()) {
                    $.ajax({
                        method: "POST",
                        url: '/dashboard/OPAPI/index.php',
                        dataType: 'json',
                        data: {
                            controller: 'Reset',
                            action: 'verifyToken',
                            data: {
                                email: $('#inputEmail').val(),
                                password: $('#password1').val(),
                                token: $('#token').val()
                            }
                        },
                        success: function (data) {
                            if (data.success) {
                                alert("Password has been reset");
                                window.location.replace("index.php");
                            }
                            else {
                                alert(data.errormsg);
                            }
                        }
                    });
                }
            })
        }
    </script>

    <title>Quantum Dashboard</title>
</head>

<body>

<div class="container" style="background:#fafafa;">
    <div class="row">
        <div class="col-lg-12">
            <img  class="fullWidth" src="assets/2013-Objectivity-Plus-Registered.png">
        </div>
    </div>

    <br><br>

    <div class="row">
        <div class="col-lg-12">
            <form class="form-signin">
                <h2 class="form-signin-heading">Enter your Email</h2>
                <label for="inputEmail" class="sr-only">Email address</label>
                <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                <button id="submit" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
            <br>
        </div>
    </div>

    <div class="Row">
        <div id="form2" class="col-lg-12">
        </div>
    </div>

</div>

</body>
</html>
