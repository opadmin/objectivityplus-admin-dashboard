<?php
require_once '../scripts/php/session.php';
include '../scripts/php/scenarios.php';
include '../scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="../scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script  src="../scripts/socket.io.js" class=""></script>
    <script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="../scripts/Cheshire.js" class=""></script>
    <script src="../scripts/bootstrap/js/bootstrap.min.js" class=""></script>
    <script src="../scripts/javascript/scenarioManagement.js" type="text/javascript"></script>
    <script src="../scripts/javascript/loadOrganizations.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(".ScenarioBlock .EditBtn").on('click', function(){

            $.ajax({
                method: "POST",
                url: '../scripts/php/scenarios/edit.php',
                dataType: 'json',
                data: {
                    id: $(this).parents(".ScenarioBlock").attr("id")
                },
                success: function(data){
                }
            });
        });

        $(".ScenarioBlock .DeleteBtn").on('click', function() {

            $.ajax({
                method: "POST",
                url: '../scripts/php/scenarios/delete.php',
                dataType: 'json',
                data: {
                    id: $(this).parents(".ScenarioBlock").attr("id")
                },
                success: function (data) {
                }
            });
        });
    </script>

    <title>Session Management</title>
</head>

<body onload="warning();">

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img class="fullWidth" src="../../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="../assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>
    <br>


    <form class="form-horizontal" enctype="multipart/form-data" method="post">
        <fieldset>

            <!-- Form Name -->
            <legend>Select Options</legend>

            <?php loadPlans(); ?>

            <div class="form-group lgPadLeft">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>

            <div class="row">
                <div class="OPBlue">
                    &nbsp;
                </div>
            </div>

            <?php filterResults(); ?>

        </fieldset>
    </form>


    <div class="row mdPadBot">
        <div id="insertHere" class="fullWidth">

        </div>
    </div>

    <!-- Modal -->
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Session</h4>
      </div>
      <div id="insertModalForm" class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

    <?php getLowerNav(); ?>

</div>

</body>
</html>
