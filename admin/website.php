<?php
require_once '../scripts/php/session.php';
include '../scripts/php/website.php';
include '../scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="../scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../scripts/socket.io.js" class=""></script>
    <script src="../scripts/Cheshire.js" class=""></script>
    <script src="../scripts/bootstrap/js/bootstrap.min.js" class=""></script>

    <script src="../scripts/javascript/siteManagement.js" class=""></script>

    <title>User Management</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img class="fullWidth" src="../../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="../assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>
    <br>

    <div class="row mdPadLeft">
        <h2>Instructions:</h2>
        <ol>
            <li>Select a page to edit</li>
            <li>Select a section (particular bio, FAQ, etc.) to edit</li>
            <li>Make changes as desired</li>
            <li>Submit changes</li>
            <li>Check production or test to make sure it's what you wanted.</li>
        </ol>

        Note: Production and test are updated immediately upon submitting
    </div>

    <form class="form-horizontal lgPadTop" method="post">
        <fieldset>

            <!-- Form Name -->
            <legend>Select Page</legend>

            <?php baseForm(); ?>

            <div class="form-group">
                <label id="SecondLabel" class="col-md-4 control-label" for="SelectBio">Select Page</label>
                <div class="col-md-4">
                    <select id="SelectQuestion" name="SelectQuestion" class="form-control" onchange="this.form.submit()">'
                    </select>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <button id="submit" name="singlebutton" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </fieldset>
    </form>
    

    <div id="insertForm">
    </div>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

    <?php getLowerNav(); ?>

</div>

</body>
</html>
