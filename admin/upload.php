<?php
require_once '../scripts/php/session.php';
include '../scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="../scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../scripts/socket.io.js" class=""></script>
    <script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="../scripts/Cheshire.js" class=""></script>
    <script src="../scripts/bootstrap/js/bootstrap.min.js" class=""></script>
    <script src="../scripts/javascript/loadOrganizations.js" type="text/javascript"></script>

    <script>
        function warning() {
            alert("This page is currently under construction and will not function as it appears. Feel free to look around");
        }
    </script>

    <title>Mass Upload Tool</title>
</head>

<body onload="warning();">

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img class="fullWidth" src="../../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="../assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>
    <br>


    <form class="form-horizontal" method="post">
        <div class="row text-center">
            <h2>Download a Template</h2>
            <div class="btn-group btn-group-lg mdPadBot">
                <a type="submit" href="../assets/user_list.csv" class="btn btn-default">User Template</a>
                <a type="submit" href="../assets/OPLibrary.xlsx" class="btn btn-default">Authoring Tool</a>
            </div>
        </div>
        <fieldset>
            <!-- Form Name -->
            <legend>Mass Upload Tool</legend>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Select Organization</label>
                <div class="col-md-4">
                    <select id="SelectOrganization" name="SelectOrganization" class="form-control">
                    </select>
                </div>
            </div>

            <!-- Multiple Radios (inline) -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="radios">Upload type</label>
                <div class="col-md-4">
                    <label class="radio-inline" for="radios-0">
                        <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
                        Upload Users
                    </label>
                    <label class="radio-inline" for="radios-1">
                        <input type="radio" name="radios" id="radios-1" value="2">
                        Upload Authoring Tool
                    </label>
                </div>
            </div>

            <!-- File Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="filebutton">File to upload</label>
                <div class="col-md-4">
                    <input id="filebutton" name="filebutton" class="btn input-file" type="file">
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton">Upload</label>
                <div class="col-md-4">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </fieldset>
    </form>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

    <?php getLowerNav(); ?>

</div>

</body>
</html>
