<?php
require_once '../scripts/php/session.php';

include '../scripts/php/scenarios.php';
include '../scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="../scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../scripts/socket.io.js" class=""></script>
    <script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="../scripts/Cheshire.js" class=""></script>
    <script src="../scripts/bootstrap/js/bootstrap.min.js" class=""></script>
    <script src="../scripts/javascript/loadOrganizations.js" type="text/javascript"></script>

    <script>
        function warning() {
            alert("This page is currently under construction and will not function as it appears. Feel free to look around");
        }
    </script>

    <title>Score Management</title>
</head>

<body onload="warning();">

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img class="fullWidth" src="../../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="../assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>
    <br>


    <form method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="org">Select organization:</label>
            <div class="col-sm-10">
                <select class="form-control" id="SelectOrganization" name="SelectOrganization">

                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6">
                <label class="control-label col-sm-3" for="start">Start Date:</label>
                <input type="date" id="start" name="start">
            </div>

            <div class="col-md-6">
                <label class="control-label col-sm-3" for="end">End Date:&nbsp;</label>
                <input type="date" id="end" name="end">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="fullWidth OPOrange text-center whiteText smPadBot smPadTop">
            <h4>Use the boxes below to select a scenario to score</h4>
        </div>
    </div>

    <div class="row mdPadBot">
        <div class="fullWidth">
            <?php getScores(); ?>
        </div>
    </div>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

    <?php getLowerNav(); ?>

</div>

</body>
</html>
