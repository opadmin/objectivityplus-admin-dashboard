<?php
require_once '../scripts/php/session.php';
include '../scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <script src="../scripts/jquery/links.js"></script>
  <link rel="stylesheet" type="text/css" href="../style/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="../scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <script src="../scripts/sweetalert-master/dist/sweetalert.min.js"></script>
  <link rel="stylesheet" type="text/css" href="../scripts/sweetalert-master/dist/sweetalert.css">
  <script src="../scripts/socket.io.js" class=""></script>
  <script src="../scripts/Cheshire.js" class=""></script>
  <script src="../scripts/bootstrap/js/bootstrap.min.js" class=""></script>
  <script src="../scripts/javascript/loadOrganizations.js" type="text/javascript"></script>
  <script src="../scripts/javascript/conferencing.js" type="text/javascript"></script>
  <script src="../scripts/javascript/conferencingEnhancements.js" type="text/javascript"></script>
  <script src="../scripts/javascript/notifications.js" type="text/javascript"></script>
  <script src="../scripts/bootstrap-switch-master/dist/js/bootstrap-switch.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="../scripts/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.css">
  <title>SMS Notifications</title>

  <script>
  function loadStartDate() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Conferencing',
            action:'getStartDate',
            data:{
                organization : $("#SelectOrganization").find(":selected").val()
            }
        },
        success: function (data) {
            if(data.success)
            {
              // $('#start').val(data.data);
              $("#start").datepicker({dateFormat: 'yyyy-mm-dd'}).datepicker('setDate', new Date(data.data));
              $('#dateSpinner').hide();
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
  }

  function getMessageText() {
    $.ajax({
        method: "POST",
        url: '/dashboard/OPAPI/index.php',
        dataType:'json',
        data: {
            controller:'Conferencing',
            action:'getMessageText',
            data:{
                sms : document.getElementById('messageToggle').checked
            }
        },
        success: function (data) {
            if(data.success)
            {
              $("#message").text('');
              $("#message").text(data.data);
            }
            else
            {
                alert(data.errormsg);
            }
        }
    });
  }
  </script>
</head>

<body onload="getMessageText();">

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img class="fullWidth" src="../../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="../assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>
    <br>

    <div id="Part1" class="row">
        <div class="col-lg-6">
            <h3>Instructions: Part 1</h3>
            <ol>
                <li>Select an organization</li>
                <li>Select a date range</li>
                <li>Continue to Part 2</li>
            </ol>
        </div>
        <form class="form-inline col-lg-6" method="post">
            <fieldset>
                <!-- Form Name -->
                <legend>Select Organization & Date Range</legend>

                <!-- Select Basic -->
                <div class="">
                    <label class="col-md-4 control-label" for="SelectOrganization">Select Organization</label>
                    <div class="input-group col-md-6">
                        <select id="SelectOrganization" name="SelectOrganization" class="form-control col-md-10" onchange="loadStartDate();">
                        </select>
                        <div id="orgSpinner" class="input-group-addon">
                          <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                        </div>
                    </div>
                </div>

                <div class="">
                    <label class="control-label col-md-4" for="start">Start Date:</label>
                    <div class="input-group col-md-6">
                        <input type="text" id="start" name="start">
                        <div id="dateSpinner" class="input-group-addon">
                          <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                        </div>
                    </div>

                </div>

                <div class="">
                    <label class="control-label col-md-4" for="end">End Date:&nbsp;</label>
                    <div class="input-group col-md-6">
                        <input type="text" id="end" name="end">
                    </div>
                </div>

                <br/>

                <!-- Button -->
                <div class="">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="input-group col-md-4">
                        <button id="submit" name="singlebutton" class="btn btn-opblue">Submit</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

    <span id="Part2line" class="sexy_line"></span>

    <div id="Part2" class="row hidden">
        <div class="col-lg-6">
            <h3>Instructions: Part 2</h3>
            'Available Dates' has been populated with every day the selected organization has at least one scenario scheduled
            <ol>
                <li>Click the dates you wish to start notifying</li>
                <li>Hint: Hold 'Ctrl' or 'Shift' to select multiple dates or "click, hold, and drag the house" accross your selections</li>
            </ol>
        </div>

        <form class="form-horizontal col-lg-6" method="post">
            <fieldset>
                <!-- Form Name -->
                <legend>Select Dates</legend>

                <div class="form-group">
                    <label class="control-label col-md-4" for="testBox">Available Dates:</label>
                    <div class="col-md-4">
                        <select multiple id='insertSelection'>
                        </select>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <button id="submitDates2" name="singlebutton" class="btn btn-opblue">Submit</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>

    <span id="Part3line" class="sexy_line"></span>

    <div id="Part3" class="row hidden">
        <div id="sessParent" class="col-md-4 col-md-offset-1 lgPadBot">
            <div class="row mdPadTop mdPadBot" style="font-size: 1.4em; font-weight: 700;">&nbsp; Scheduled Sessions &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <button type="button" class="btn btn-opblue allButton">Notify All</button>
            </div>

          <div id="Sessions" class="row">

          </div>
        </div>

        <div class="col-md-6 col-md-offset-1">
          <form class="form-horizontal">
            <fieldset>

            <!-- Form Name -->
            <legend>Message</legend>

            <!-- Multiple Checkboxes (inline) -->
            <div class="form-group">
            <label class="col-md-4 control-label" for="smscheck">Email/SMS Toggle</label>
            <div class="col-md-4">
              <input id="messageToggle" type="checkbox" data-on-text="SMS" data-off-text="Email" data-off-color="info" data-size="small" name="my-checkbox" onSwitchChange="getMessageText();" checked>
            </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
            <label class="col-md-4 control-label" for="message">Message</label>
            <div class="col-md-4">
            <textarea style="margin: 0px -157.538px 0px 0px; width: 322px; height: 180px;" class="form-control" id="message" name="message"></textarea>
            </div>
            </div>

            </fieldset>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

    <?php getLowerNav(); ?>

</div>

</body>
</html>
