<?php
include '../scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="../scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../scripts/socket.io.js" class=""></script>
    <script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="../scripts/Cheshire.js" class=""></script>
    <script src="../scripts/bootstrap/js/bootstrap.min.js" class=""></script>
    <script src="../scripts/javascript/judgingPlan.js" type="text/javascript"></script>
    <script src="../scripts/javascript/loadOrganizations.js" type="text/javascript"></script>

    <title>User Management</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img class="fullWidth" src="../../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="../assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>
    <br>

    <form class="form-horizontal">
        <fieldset>

            <!-- Form Name -->
            <legend>Select Judging Plan</legend>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="organization">Select Organization</label>
                <div class="col-md-4">
                    <select id="SelectOrganization" name="SelectOrganization" type="text" placeholder="" class="form-control input-md" onchange="reloadPlans();"></select>
                </div>
            </div>

            <!-- Multiple Checkboxes -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="choices">Select statuses to ignore</label>
                <div class="col-md-4">
                    <div class="checkbox">
                        <label for="choices-0">
                            <input type="checkbox" name="choices" id="choices-0" value="approve" onchange="reloadPlans();">
                            approve
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="choices-1">
                            <input type="checkbox" name="choices" id="choices-1" value="create" onchange="reloadPlans();">
                            create
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="choices-2">
                            <input type="checkbox" name="choices" id="choices-2" value="runningScoring" onchange="reloadPlans();">
                            runningScoring
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="choices-3">
                            <input type="checkbox" name="choices" id="choices-3" value="cancel" onchange="reloadPlans();">
                            cancel
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="choices-4">
                            <input type="checkbox" name="choices" id="choices-4" value="complete" onchange="reloadPlans();">
                            complete
                        </label>
                    </div>
                </div>
            </div>

            <div id="planSelector" class="form-group">
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="submit"></label>
                <div class="col-md-4">
                    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </fieldset>
    </form>


    <div class="row">
        <div class="fullWidth OPOrange text-center whiteText smPadBot smPadTop">
            <h4>Edit the Plan:</h4>
        </div>
    </div>

    <div id="alterForm">
    </div>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

    <?php getLowerNav(); ?>

</div>

</body>
</html>
