<?php
require_once '../scripts/php/session.php';
include '../scripts/php/organizations.php';
include '../scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="../scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="../scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../scripts/socket.io.js" class=""></script>
    <script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="../scripts/Cheshire.js" class=""></script>
    <script src="../scripts/bootstrap/js/bootstrap.min.js" class=""></script>
    <script src="../scripts/javascript/manageUsers.js" type="text/javascript"></script>
    <script src="../scripts/javascript/loadOrganizations.js" type="text/javascript"></script>

    <title>User Management</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img class="fullWidth" src="../../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="../assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>
    <br>

    <form class="form-horizontal">
        <fieldset>

            <!-- Form Name -->
            <legend>Select User</legend>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Select Organization</label>
                <div class="col-md-4">
                    <select id="SelectOrganization" name="SelectOrganization" class="form-control" onchange="findUsersforOrg()">
                    </select>
                </div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Select User</label>
                <div class="col-md-4">
                    <select id="SelectUser" name="SelectUser" class="form-control" onchange="pullUsers()">
                    </select>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <button id="submit" name="singlebutton" class="btn btn-primary">Submit</button>
                    <button id="submitnewUser" style="margin-left: 35%;" data-toggle="modal" data-target="#userModal" name="singlebutton" class="btn btn-primary">Add New User</button>
                </div>
            </div>

        </fieldset>
    </form>

    <form class="form-horizontal">
        <fieldset>

            <!-- Form Name -->
            <legend>Search for User</legend>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="username_search">Email</label>
                <div class="col-md-4">
                    <input id="username_search" name="username_search" type="text" placeholder="kala@thekitten.com" class="form-control input-md">

                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="search"></label>
                <div class="col-md-4">
                    <button id="search" name="search" class="btn btn-primary">Search</button>
                </div>
            </div>

        </fieldset>
    </form>


    <div class="row">
        <div class="fullWidth OPOrange text-center whiteText smPadBot smPadTop">
            <h4>Edit the User:</h4>
        </div>
    </div>

    <div id="alterForm">
    </div>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

</div>

<!-- Modal -->
<div id="userModal" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">New User</h4>
  </div>
  <div id="insertModalForm" class="modal-body">
    <form class="form-horizontal">
      <fieldset>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="first_name">First Name</label>
        <div class="col-md-4">
        <input id="first_name" name="first_name" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="middle_name">Middle Name</label>
        <div class="col-md-4">
        <input id="middle_name" name="middle_name" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="last_name">Last Name</label>
        <div class="col-md-4">
        <input id="last_name" name="last_name" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="email">Email</label>
        <div class="col-md-4">
        <input id="email" name="email" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
          <label class="col-md-4 control-label" for="phone">Phone</label>
          <div class="col-md-4">
              <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md">

          </div>
      </div>

      <!-- Select Basic -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="role">Select Role</label>
        <div class="col-md-4">
          <select id="role" name="role" class="form-control">
            <option value="6">Learner</option>
            <option value="7">Adjunct Instructor</option>
            <option value="22">Lead Instructor</option>
            <option value="9">Admin</option>
          </select>
        </div>
      </div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="submitNewUser"></label>
        <div class="col-md-4">
          <button id="submitNewUser" name="submitNewUser" class="btn btn-primary">Submit</button>
        </div>
      </div>

      </fieldset>
      </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
</div>

</div>
</div>

</body>
</html>
