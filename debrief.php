<?php
require_once 'scripts/php/session.php';
include 'scripts/php/Navigation.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="scripts/sweetalert-master/dist/sweetalert.min.js"></script>
    <script src="scripts/sweetalert-master/dist/sweetalert-dev.js"></script>
    <link rel="stylesheet" type="text/css" href="scripts/sweetalert-master/dist/sweetalert.css">
    <script src="scripts/socket.io.js" class=""></script>
    <script src="scripts/Cheshire.js" class=""></script>
    <script src="scripts/bootstrap/js/bootstrap.min.js" class=""></script>
    <script src="scripts/javascript/loadOrganizations.js" type="text/javascript"></script>
    <script src="scripts/javascript/bootstrap-notify.js" type="text/javascript"></script>
    <script src="scripts/javascript/bootstrap-notify.min.js" type="text/javascript"></script>

    <title>Debrief IT</title>

    <script>


        $(document).ready(function(e) {

            //initialize datepicker
            $('#start, #end').datepicker();
            $("#end").datepicker('setDate', new Date());

            $('#submit').on('click', function(e) {
                $(this).prop("disabled", true).html("<i class='fa fa-spin fa-spinner'></i>");

                e.preventDefault();
                $.ajax({
                    method: "POST",
                    url: '/dashboard/OPAPI/index.php',
                    dataType: 'json',
                    data: {
                        controller: 'Conferencing',
                        action: 'pullCompletedStudents',
                        data: {
                            organization: $("#SelectOrganization").find(":selected").attr("id"),
                            startDate: $('#start').datepicker({
                                dateFormat: 'mm/dd/yyyy'
                            }).val(),
                            endDate: $('#end').datepicker({
                                dateFormat: 'mm/dd/yyyy'
                            }).val()
                        }
                    },
                    success: function(data) {

                        $("#submit").prop("disabled", false).html("Submit");

                        if (data.success) {
                            $("#insertEvaluations").html('<option>No Selection</option>');

                            for (var i = 0; i < data.data.length; ++i) {
                                var opt = "<option data-name='" + data.data[i].name +"' data-user='" + data.data[i].user_id + "' id='" + data.data[i].id + "'>" + data.data[i].name + ", " + data.data[i].start_datetime +"</option>";
                                $("#insertEvaluations").append(opt);
                            }

                            $('#Part2, #Part2line').fadeIn("slow", function() {}).removeClass("hidden");
                        } else {
                            swal("Error", data.errormsg, "error");
                        }
                    }
                });
            });

            $('#insertEvaluations').on("change", function(e) {
                e.preventDefault();

                $.ajax({
                    method: "POST",
                    url: '/dashboard/OPAPI/index.php',
                    dataType:'json',
                    data: {
                        controller:'Conferencing',
                        action:'loadResults',
                        data:{
                            session : $("#insertEvaluations").find(":selected").attr("id"),
                            user_id : $("#insertEvaluations").find(":selected").attr("data-user")
                        }
                    },
                    success: function (data) {
                        if(data.success)
                        {

                            if (data.data.length == 0) {
                                swal("Results not found");
                            }

                            for (var i = 0; i < data.data.length; ++i) {
                            if (data.data.length == 1) {
                                swal("Warning: Only 1 instructor found");
                            }

                            var container = "<div ";
                            for(var j = 0; j < data.data[i].length; ++j) {
                                if (j == 0) {
                                    container += "id='" +  data.data[i][j].id + "' class='col-md-6 Evaluation'><div class='row evalHeader'>Instructor: " + data.data[i][j].name + "</div><div class='col-md-12 fullWidth nopaddingStrict evalBody'>";
                                }

                                container += "<div class='row smPadTop smPadBot criteria" + data.data[i][j].checked + "'><div id='" + data.data[i][j].checked + "' class='col-md-11'>"
                                    + data.data[i][j].text + "</div><div class='col-md-1 nopadding'><i class='fa fa-plus-square fa-lg' aria-hidden='true'></i>&nbsp;&nbsp;<i class='fa fa-pencil-square fa-lg hidden' aria-hidden='true'></i></div></div>";

                            }
                            container += "</div>";
                            $('#studentName').html("Results for: " + $("#insertEvaluations").find(":selected").attr("data-name"));
                            $('#Part3').removeClass("hidden");
                            $('#evaluations').append(container);

                            }


                        }
                        else
                        {
                            swal("Error", data.errormsg, "error");
                        }
                    }
                });
            });

            $("body").on("click", ".fa-plus-square", function(e) {
                swal("This will be a note", "Sorry, this functionality hasn't been implemented yet!");
            });

            $("body").on("click", ".fa-pencil-square", function(e) {

            });
            
        });

        function loadStartDate() {
            $.ajax({
                method: "POST",
                url: '/dashboard/OPAPI/index.php',
                dataType:'json',
                data: {
                    controller:'Conferencing',
                    action:'getStartDate',
                    data:{
                        organization : $("#SelectOrganization").find(":selected").val()
                    }
                },
                success: function (data) {
                    if(data.success)
                    {
                        $("#start").datepicker({dateFormat: 'yyyy-mm-dd'}).datepicker('setDate', new Date(data.data));
                        $('#dateSpinner').hide();

                        $("#submit").prop("disabled", false).html("Submit");
                    }
                    else
                    {
                        swal("Error", data.errormsg, "error");
                    }
                }
            });
        }

        function loadResults() {

        }
    </script>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img  class="fullWidth" src="../assets/OPBanner.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="assets/Spy-01.png">
        </div>
    </div>
    <?php getNavigation(); ?>

    <br>

    <div id="mainContainer" class="col-md-12">
        <div class="row text-center">

        </div>
        <br/>

        <div class="row">
            <div id="Part1" class="Part">
                <form class="conference-filter-control form-inline col-lg-6" method="post">
                    <fieldset>
                        <!-- Form Name -->
                        <h3>Select Organization & Date Range</h3>

                        <!-- Select Basic -->
                        <div class="">
                            <label class="col-md-4 control-label" for="SelectOrganization">Select Organization</label>
                            <div class="input-group col-md-6">
                                <select id="SelectOrganization" name="SelectOrganization" class="form-control col-md-10" onchange="loadStartDate();">
                                </select>
                                <div id="orgSpinner" class="input-group-addon">
                                    <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <label class="control-label col-md-4" for="start">Start Date:</label>
                            <div class="input-group col-md-6">
                                <input class="form-control" type="text" id="start" name="start">
                                <div id="dateSpinner" class="input-group-addon">
                                    <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i>
                                </div>
                            </div>

                        </div>

                        <div class="">
                            <label class="control-label col-md-4" for="end">End Date:&nbsp;</label>
                            <div class="input-group col-md-6">
                                <input class="form-control" type="text" id="end" name="end">
                            </div>
                        </div>

                        <br/>

                        <!-- Button -->
                        <div class="">
                            <label class="col-md-4 control-label" for="singlebutton"></label>
                            <div class="input-group col-md-4">
                                <button id="submit" name="singlebutton" class="btn btn-opblue" disabled>Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>

            <br/>

            <div id="Part2" class="hidden Part">
                <form class="conference-filter-control form-horizontal col-lg-6" method="post">
                    <fieldset>
                        <!-- Form Name -->
                        <h3>Select an Evaluation</h3>

                        <div class="form-group">
                            <label class="control-label col-md-4" for="testBox">Evaluations:</label>
                            <div class="col-md-8">
                                <select style="width: auto;" class="form-control col-md-10" id='insertEvaluations' >
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

        <br/>

        <div id="Part3" class="hidden row">
            <div class="row text-center OPOrangeD1 mdPadTop mdPadBot">
                <div style="font-weight: 600; font-size: 1.4em;" id="studentName">Results for: </div>
            </div>

            <br/>

            <div id="evaluations" class="row">
            </div>
        </div>


        <br />

        <?php getLowerNav(); ?>

    </div>

</div>

</body>
</html>
