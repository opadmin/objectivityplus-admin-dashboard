<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 4/4/2016
 * Time: 11:52 AM
 */

require 'vendor/autoload.php';
use Mailgun\Mailgun;


$_host = "db2.apexinnovations.com";

$connection = new PDO("mysql:host={$_host};dbname=appquantumdb;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');

/**     Enable exceptions    **/
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$query = 'SELECT org.name, jud.id, jud.created_datetime, jud.code
            FROM appquantumdb.judging_plan				jud
            INNER JOIN appquantumdb.organization		org
                ON jud.organization_id = org.id
            LEFT JOIN appquantumdb.judging_plan_session	sess
                ON sess.judging_plan_id = jud.id
            LEFT JOIN appquantumdb.judging_plan_session_result	res
                ON sess.id = res.session_id
            WHERE jud.status = "runningScoring"
            AND res.id IS NOT NULL
            GROUP BY org.name, jud.id, jud.created_datetime;';


$command = $connection->prepare($query);
$command->execute();
$unscoredScenarios = $command->fetchAll(PDO::FETCH_ASSOC);

$mgClient = new Mailgun('key-ca326a478754b7447e178fdb37fc2be0');
$domain = "objectivityplus.com";
$html = '<html>
            <head><style>
table {
    border-collapse: collapse;
    width: 60%;
}

th, td {
    text-align: left;
    padding: 7px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style></head>
            <h3>The following scenarios have been paid for, completed, and await scoring:</h3>
            <table>
              <tr>
                <th>Organization</th>
                <th>Judging Plan ID</th>
                <th>Date Created</th>
                <th>Code</th>
               </tr>
              ';

foreach ($unscoredScenarios as $uS)
{
        $html .=  '<tr> <td>' . $uS['name'] . '</td> <td>' . $uS['id'] . '</td> <td>' . $uS['created_datetime'] . '</td> <td>' . $uS['code'] . '</td> </tr>';

}

$html .= '</table> </html>';

# Make the call to the client.
$result = $mgClient->sendMessage($domain, array(
    'from'    => 'Dero Kratzberg <dero@objectivityplus.com>',
    'to'      => 'Developers <developers@objectivityplus.com>, Stephen Hetherman <steve@objectivityplus.com>',
    'subject' => 'Automated Unscored Scenarios',
    'html'    => $html
));
