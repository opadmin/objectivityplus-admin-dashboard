<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <link href='https://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script async="true" src="scripts/socket.io.js" class=""></script>
    <script src="//d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="scripts/Cheshire.js" class=""></script>
    <script src="scripts/bootstrap/js/bootstrap.min.js" class=""></script>

    <title>Quantum Dashboard</title>
</head>

<body>

<div class="container" style="background:#fafafa;">
    <div class="row">
        <div class="col-lg-12">
            <img  class="fullWidth" src="assets/2013-Objectivity-Plus-Registered.png">
        </div>
    </div>

    <br><br>

    <div class="row">
        <div class="col-lg-12">
            <form class="form-signin" method="post" action="scripts/php/login.php">
                <h2 class="form-signin-heading">Please Sign In</h2>
                <label for="inputEmail" class="sr-only">Email address</label>
                <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
            <br>
            Lost in thought? Strayed from your path in life? Generally disassociated in your own paradigm? <a href="reset.php">Try a password reset</a>
        </div>

    </div>

</div>

</body>
</html>

