<?php
/**
 * Created by PhpStorm.
 * User: dero
 * Date: 8/23/2016
 * Time: 09:50
 */
require 'vendor/autoload.php';
require 'vendor/cboden/ratchet/Sockets.php';

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
//use Ratchet\Sockets;

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Sockets()
        )
    ),
    8080
);

$server->run();