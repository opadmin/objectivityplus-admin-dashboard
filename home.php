<?php
require_once 'scripts/php/session.php';
require 'scripts/php/Navigation.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="scripts/jquery/links.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="scripts/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="scripts/socket.io.js" class=""></script>
    <script src="scripts/Cheshire.js" class=""></script>
    <script src="scripts/bootstrap/js/bootstrap.min.js" class=""></script>

    <title>Quantum Dashboard</title>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <img  class="fullWidth" src="../assets/2013-Objectivity-Plus-Registered.png">
        </div>
        <div class="col-lg-3 nopadding hidden-md hidden-sm hidden-xs">
            <img class="fullWidth" id="Logo" src="assets/Spy-01.png">
        </div>
    </div>

    <?php getNavigation(); ?>

    <div class="row mdPadTop">
        <div class="col-xs-12 col-md-6">
             Welcome to the new ObjectivityPlus backend dashboard. Here you will find the necessary tool to manage the database
            underlying Quantum. The necessary steps for any modification are designed to be intuitive, but Dero is quite happy to provide
            guidance if necessary. The basic functions available here right now are:
            <ul>
                <li>Modify user information</li>
                <li>Modify scenarios and scheduling</li>
                <li>Manage payment (credits) status</li>
                <li>Manage organizational color schemes</li>
                <li>Quickly manage user schedule assignments</li>
            </ul>
        </div>

        <div class="col-xs-12 col-md-6">
            As a quick guide, this will help you navigate the site:
            <ul>
                <li><i class="fa fa-home fa-4x grayText"></i>: This is the home page</li>
                <li><i class="fa fa-tachometer fa-4x grayText"></i>: This is the analytics page. You'll find plots, charts, and statistics here. Reports too, eventually</li>
                <li><i class="fa fa-terminal fa-4x grayText"></i>: This is the admin page for Quantum. You can modify database objects here. This is the original functionality</li>
                <li><i class="fa fa-briefcase fa-4x grayText"></i>: This is the conferencing tool. It allows for quick assignment of users to schedules</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="fullWidth OPBlue">
            &nbsp;
        </div>
    </div>

    <?php getLowerNav(); ?>


</div>

</body>
</html>
