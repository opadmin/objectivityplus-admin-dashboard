<?php
/**
 * Created by PhpStorm.
 * User: Dero
 * Date: 4/4/2016
 * Time: 11:52 AM
 */

require 'vendor/autoload.php';
use Mailgun\Mailgun;


$_host = "db2.apexinnovations.com";

$connection = new PDO("mysql:host={$_host};dbname=Quantum;", 'opadmin', 'A5rEwombo2i4wWekdfrF') or die ('Can not connect to database');

/**     Enable exceptions    **/
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$query = 'SELECT id, open_date, user_name, user_email
            FROM Quantum.tickets
            WHERE status != "CLOSED";';


$command = $connection->prepare($query);
$command->execute();
$tickets = $command->fetchAll(PDO::FETCH_ASSOC);

$mgClient = new Mailgun('key-ca326a478754b7447e178fdb37fc2be0');
$domain = "objectivityplus.com";
$html = '<html>
            <head><style>
table {
    border-collapse: collapse;
    width: 60%;
}

th, td {
    text-align: left;
    padding: 7px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style></head>
            <h3>The following tickets remain open:</h3>
            <table>
              <tr>
                <th>User</th>
                <th>Ticket ID</th>
                <th>Date Created</th>
                <th>User Email</th>
               </tr>
              ';

foreach ($tickets as $uS)
{
        $html .=  '<tr> <td>' . $uS['user_name'] . '</td> <td>' . $uS['id'] . '</td> <td>' . $uS['open_date'] . '</td> <td>' . $uS['user_email'] . '</td> </tr>';

}

$html .= '</table> </html>';

# Make the call to the client.
$result = $mgClient->sendMessage($domain, array(
    'from'    => 'Developers <developers@objectivityplus.com>',
    'to'      => 'Developers <developers@objectivityplus.com>, Stephen Hetherman <steve@objectivityplus.com>',
    'subject' => 'Automated Open Support Tickets',
    'html'    => $html
));
